/*  
    Author: Paul Horton
    Creation Date: 2017.3.20

    Description: Class to hold information about parameter which can search over a fixed set of values.

    Context:  Created as part of a simulation of diffusion molecules.
 */
#pragma once
#include "utils.hh"


class EnumdParam{
public:

  /*───────────────  CONSTRUCTORS  ───────────────*/
  static EnumdParam make(  const string initData  );

  EnumdParam(  const string& name,  const doubleVecT& vals  )
    :
    _name( name ), _vals(vals), _curI(0)
  {}

  /*───────────────  ACCESSORS  ───────────────*/
  const string& name() const{  return _name;  }

  double operator()() const{   return _vals.at(_curI);   }

  void reset(){  _curI= 0;  }

  bool inRangeP(){   return _curI < _vals.size();   }

  void assertAllValidProbs(){
    for(  size_t i= 0;  i < _vals.size();  ++i  ){
      ASSERTF(  utils::inValidProbRangeP(  _vals[i]  ),
                "EnumdParam %s expected all values in vals to be ∈ [0,1], but val[%zu]=%g\n",  C(_name), i, _vals[i]  );
    }
  }


  friend ostream& operator<<(ostream&, const EnumdParam&);


  /*───────────────  OTHER METHODS  ───────────────*/
  void next(){  ++_curI;  }


  /*───────────────  OBJECT DATA  ───────────────*/
private:
  const string      _name;
  const doubleVecT  _vals;
  /* */ size_t      _curI;
};


inline  ostream& operator<<(  ostream& os,  const EnumdParam& param  ){
  os  <<  param._name;
  for(  const auto& val :  param._vals  ){   os  <<  ':' << val;   }
  return  os;
}
