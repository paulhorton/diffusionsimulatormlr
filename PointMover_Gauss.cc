/*
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180318
 *  Updated: 20180318
 *  Description: See header file.
 */
#include "PointMover_Gauss.hh"



void PointMover_Gauss::move(  Point& point  ){

  double newX, newY, newZ;
  point.copyCoordsTo(  newX, newY, newZ  );

  newX +=  gauss( randomBitGenerator );
  newY +=  gauss( randomBitGenerator );
  newZ +=  gauss( randomBitGenerator );

  point.moveTo(  newX, newY, newZ  );
}

