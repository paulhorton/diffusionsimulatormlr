/*  
 *  Author: Paul Horton
 *  Creation Date: 20170317
 *
 *  Description: Abstact class to move a point one small step in 3D space
 *
 *  Purpose:  Created as part of diffusion simulation in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "Point.hh"


class PointMover{
public:

  virtual  void set_MSDperMove( double dist )=  0;

  // "const" in spirit, but changes state of random number generator (e.g. std::normal_distribution)
  virtual void move(  Point& point  )=  0;   
};
