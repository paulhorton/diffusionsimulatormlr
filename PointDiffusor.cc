
/*
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180318
 *  Updated: 20180318
 *  Description: See header file.
 */
#include "utils.hh"
#include "diffusionCoefficientEquations.hh"
#include "PointDiffusor.hh"
#include "PointMover_Gauss.hh"
#include "PointMover_fixed.hh"
#include "PointMover_1fixed.hh"



void PointDiffusor::setDiffusionCoeff(  const double DC  ){
  DChasBeenSetP=  true;

  if( varyTimeP )   timePerStep_=  diffusion::timePerStep(  DC, MSDperStep_   );
  else              MSDperStep_ =  diffusion::MSDperStep (  DC, timePerStep_  );

  pointMover_->set_MSDperMove( MSDperStep_ );

}



static stringDQ_T  specWords;

bool ateSpecWordP(  const string& word  );
bool ateSpecWordP(  const string& word  ){
  return
    utils::deletedOneFromStringsP( specWords, word );
}


PointDiffusor::PointDiffusor(  const string diffusorSpec,  const double granularity  )
{
  ASSERTF(  granularity > 0,  "PointDiffusor constructor expected positive value for granularity but got: %g\n",  granularity  );

  string spec=  diffusorSpec;
  for(  auto& c  :  spec  ){   c= tolower(c);   }

  specWords=  utils::splitOnColons( spec );

 
  /*──────────  Set up time versus distance adjustment  ──────────*/
  varyTimeP=  ateSpecWordP( "varydist" )
    ?  false
    :  ateSpecWordP( "varytime" );

  if(  varyTimeP  )   MSDperStep_ =  granularity;
  else                timePerStep_=  granularity;


  /*──────────  Get appropriate PointMover  ──────────*/
  /**/ if(  ateSpecWordP( "gauss" )  ){
    pointMover_=  new PointMover_Gauss();
  }
  else if(  ateSpecWordP( "fixed" )  ){
    pointMover_=  new PointMover_fixed();
  }
  else if(  ateSpecWordP( "fixed1" )  ){
    pointMover_=  new PointMover_1fixed( );
  }

  if(  specWords.size()  ){
    cout  <<  "Could not parse diffusorSpec '"  <<  diffusorSpec  <<  "'.  Unparser words were:\n";
    for(  auto& s  :  specWords  ){   cout  <<  "  "  <<  s  <<  endl;   }
    cout  <<  PointDiffusor::usage_diffusorSpec()  <<  endl;
    exit( 1 );
  }
    
}//  PointDiffusor::PointDiffusor( diffusorSpec, timePerStep  )


