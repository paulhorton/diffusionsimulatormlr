/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  PH:20171125 Replaced by StepLengthComputer.hh
 *
 */
#pragma once


namespace simulator{

  double constexpr average_mRNA_halfLife_inSecs=  660;

  static double numStepsPerSec_=  1000.0;
  inline double numStepsPerSec(){   return numStepsPerSec_;   }

  inline double stepTimeSpan_inSecs(){   return  1.0 / numStepsPerSec();   }

  inline double stepsPerHalfLifeWalk(){
    return   average_mRNA_halfLife_inSecs / stepTimeSpan_inSecs();
  }

  double inline
  diffusionCoefficient_ofStrideLength(  const double strideLength,
                                        const double myStepTimeSpan= stepTimeSpan_inSecs()  ){
    return   strideLength * strideLength / myStepTimeSpan / 6.0;
  }


  double inline
  strideLength_ofDiffusionCoefficient(  const double diffusionCoefficient,
                                        const double myStepTimeSpan= stepTimeSpan_inSecs()  ){
    return  sqrt(  diffusionCoefficient * 6.0 * myStepTimeSpan  );
  }

}
