/*  
 *  Author: Paul Horton
 *  Creation Date: 20170319
 *
 *  Description: Implementation of PointMover which
 *               moves the point according to a normal distribution
 *               along each axis.
 *
 */
#pragma once
#include <random>
#include "sharedRandomSeed.hh"
#include "PointMover.hh"


class PointMover_Gauss : public PointMover{
public:
  PointMover_Gauss(){
    randomBitGenerator.seed(  sharedRandomSeed()  );
  }

  void move(  Point& point  ) override;

  void set_MSDperMove(  double MSDperMove  ) override{
    std::normal_distribution<>::param_type  param {0.0,  sqrt( MSDperMove /3.0 )};
    gauss.param(  param  );
  }


private:
  std::default_random_engine randomBitGenerator{};
  std::normal_distribution<> gauss;
};
