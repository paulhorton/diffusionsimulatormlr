//  This code is a rewrite by Paul Horton of an earlier program by Thomas Poulsen.
#include "Cytoplasm.hh"


size_t Cytoplasm::mRNAinMitoP() const{
  //  Check if mRNA hit a mitochondria, if so undo the step and return the number of the mitochondria.
  for(  size_t i= 0;  i < mitos.size();  i++  ){
    if(  mitos[i].overlapsP( mRNA )  ){
      return  i+1;   //  Return number of hit mitochondria.
    }
  }

  return 0;          //  Return zero, since the mRNA hit nothing.
}


//  Randomly place mitochondria with random radius in [mitoRadiusMin, mitoRadiusMax], under the constraint that they are
//  contained in the cytoplasm and do not overlap with each other.
void Cytoplasm::randomlyPlaceMitos(){

  //  Give each mitochondria a random radius.
  for(  auto& curMito  :  mitos  ){
    curMito.setRadius(
                      utils::randomNumber_fromRange( mitoRadiusMin, mitoRadiusMax )
                      );
  }


  //  Randomly place mitochondria in non-overlapping positions in the cytosol.
  //  Rejection sampling is use to ensure no overlaps.
  for(  size_t curIdx= 0;   curIdx < mitos.size();  curIdx++  ){
    auto& curMito=  mitos[curIdx];
    
    //  Place randomly.
    curMito.placeRandomly_withinOriginCenteredSphericalShell( nuclRadius, cellRadius );

    for(  size_t prevIdx= 0;  prevIdx != curIdx;  ++prevIdx  ){
      if(  curMito.overlapsP( mitos[prevIdx] )  ){
        curIdx--;  break;  // Redo curMito if it overlaps with a previously placed mitochondria.
      }
    }
  }
}//void Cytoplasm::randomlyPlaceMitos()
