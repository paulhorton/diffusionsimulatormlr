//  See header file.
#include "sharedRandomSeed.hh"
#include "ArgvParser_forSimulator.hh"


ArgvParser_forSimulator::
ArgvParser_forSimulator(  const string& _usage,
                          const int argc, const char* argv[]
                          )
  : ArgvParser( _usage, argc, argv )
{
  sharedRandomSeed_init(   optVal ("-r", 0)   );

  const time_t currentTime=  time(NULL);
  char       timeCstr[256];
  strftime(  timeCstr,256,  "UTC %Y%m%d %T",  gmtime(&currentTime)  );

  struct utsname  uts;   //  Unix Timesharing System
  if(  uname( &uts )  ){
    cout  <<  "Could not get host information via call to uname()!\n";
    exit( 68 );
  }

  cout  <<  "# Code compiled at local time: "  <<  __DATE__   <<' '<<  __TIME__  <<  ".\n"
        <<  "# Executed  "  <<  timeCstr  <<  "  on "  <<  uts.machine  <<' '<<  uts.sysname  <<' '<<  uts.version  <<' '<<  uts.release  <<' '<<  uts.nodename  <<  endl
        <<  "# Seeded random number generator with ranSeed="  <<  sharedRandomSeed()  <<  ".\n"
        <<  "# From command line:";
  for(  const auto& s  :  argvCopy  ){   cout << ' ' << s;   }
  cout  <<  endl;

}
