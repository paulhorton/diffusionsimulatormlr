/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  PH:20171125 Replaced by Simulator.hh
 *
 */
#pragma once
#include "Cytoplasm.hh"
#include "DiffusingPoint.hh"
#include "Gene.hh"
#include "GeneStats.hh"
#include "MobilityParams.hh"
#include "simulator_basics.hh"


namespace simulator{

  void doWalks(
               /***/ Cytoplasm&       cytoplasm,
               const MobilityParams&  mobilityParams,
               const Gene&            gene,
               /***/ GeneStats&       geneStats,
               const size_t           numWalks=    1,
               const bool             dumpTraceP=  false,
               const double           timeToWalkInSecs= average_mRNA_halfLife_inSecs
               );
  
}
