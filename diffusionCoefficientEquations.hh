/*
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180408
 *  Updated: 20180408
 *
 *  Description: Some functions relating diffusion coefficient with basic quantities of a random walk simulation.
 *
 *  Abbreviations:  DC   Diffusion Coefficient
 *                  MSD  Mean Squared Displacement (distance traveled)
 */
#pragma once


namespace diffusion{
  inline
  double coefficient(  double MSDperStep,  double timePerStep  ){
    return   MSDperStep / timePerStep / 6.0;
  }

  inline
  double MSDperStep (  double DC,  double timePerStep  ){
    return   DC * timePerStep * 6.0;
  }

  inline
  double timePerStep(  double DC,  double MSDperStep  ){
    return   MSDperStep / DC / 6.0;
  }
}
