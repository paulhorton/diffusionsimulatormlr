/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  Description: Container to read and hold information on genes.
 *               Can read its data in from a tab separated values format input file.
 *
 *  Purpose: Created as a reimplementation of a random walk simulation for mRNA molecules conceived of and written by Thomas Poulsen.
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"
#include "utils.hh"
#include "Gene.hh"
#include "GeneStats.hh"
#include "MobilityParams.hh"



class Genes{
public:

  /*───────────────  CONSTRUCTORS  ───────────────*/
  Genes(  const string& inputPathname  );


  /*───────────────  ACCESSORS  ───────────────*/
  size_t size() const{   return _geneVec.size();   }

  Gene&       gene     ( const size_t idx ){  return      _geneVec.at(idx);  }
  GeneStats&  geneStats( const size_t idx ){  return _geneStatsVec.at(idx);  }

  const doubleVecT&   given_mitoLocProbs()                      const{  return _given_mitoLocProbs;          }
  /***/ double        given_mitoLocProb( const size_t idx )     const{  return _given_mitoLocProbs.at(idx);  }
  const vector<Gene>& geneVec()                const{  return _geneVec;               }

  vector<Gene>::iterator begin(){  return _geneVec.begin();   }
  vector<Gene>::iterator end  (){  return _geneVec.end  ();   }

  vector<Gene>::const_iterator begin() const{  return _geneVec.begin();   }
  vector<Gene>::const_iterator end  () const{  return _geneVec.end  ();   }

  void setNumWalks( const size_t numWalks ){  _numWalks= numWalks;  }


  /*───────────────  OTHER METHODS  ───────────────*/
  void zeroStats(){
    for(  auto& geneStats :  _geneStatsVec  )   geneStats.zeroStats();
  }


  doubleVecT simulated_mitoLocProbs() const{
    ASSERTF(  _numWalks > 0,  "_numWalks= %zu, perhaps you forgot to call setNumWalks?\n", _numWalks  );
    
    doubleVecT retVal;

    for(   const auto& geneStats  :  _geneStatsVec   ){
      retVal.push_back(   geneStats.numAnchorings()  /  (double) _numWalks  );
    }
    return retVal;
  }


  double simulationCorr() const{
    return  utils::PearsonCorr(    _given_mitoLocProbs,  simulated_mitoLocProbs()   );
  }

  double simulationMAE()  const{
    return  utils::meanAbsError(   _given_mitoLocProbs,  simulated_mitoLocProbs()   );
  }


  friend ostream& operator<<(  ostream& os,  const Genes& genes  );

  /*───────────────  OBJECT DATA  ───────────────*/
private:
  vector<Gene>       _geneVec;
  doubleVecT         _given_mitoLocProbs;
  vector<GeneStats>  _geneStatsVec;
  size_t             _numWalks;
};

