/*
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180318
 *  Updated: 20180318
 *  Description: See header file.
 */
#include "PointMover_1fixed.hh"


void PointMover_1fixed::move(  Point& point  ){

  double newX, newY, newZ;
  point.copyCoordsTo(  newX, newY, newZ  );

  const double curRandomNumber=  utils::randomNumber_fromRange(  0.0, 6.0  );

  if(  curRandomNumber < 3  ){
    /**/ if( curRandomNumber < 1 )    newX += distPerMove;
    else if( curRandomNumber < 2 )    newX -= distPerMove;
    else                              newY += distPerMove;
  }
  else{
    /**/ if( curRandomNumber < 4 )    newY-= distPerMove;
    else if( curRandomNumber < 5 )    newZ += distPerMove;
    else                              newZ -= distPerMove;
  }

  point.moveTo(  newX, newY, newZ  );
}
