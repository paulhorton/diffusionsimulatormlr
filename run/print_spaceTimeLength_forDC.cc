/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.17
 *
 *  Description: Print space and time step size suitable to simulate a stipulated diffusion coefficient.
 *
 *  Purpose: Created for mRNA diffusion simulation project with Thomas Poulsen et al.
 *           
 */
#include "../ArgvParser.hh"
#include "../StepLengthComputer.hh"


int main(  int argc,  const char* argv[]  ){
  ArgvParser argvParser(  "diffusionConstant(μm²/sec) numDimsMovedPerStep varyTime|varyGrid baseLength",  argc, argv  );

  const double DC                 = argvParser.argVal<double>(0);
  const size_t numDimsMovedPerStep= argvParser.argVal<size_t>(1);
  const string whatToVary         = argvParser.argVal<string>(2);
  const double baseLength         = argvParser.argVal<double>(3);
  argvParser.close();

  const StepLengthComputer *const stepLengthComputer=
    StepLengthComputer::newInstance( numDimsMovedPerStep, whatToVary, baseLength );

  double spaceStepLength, timeStepLength;
  stepLengthComputer->set_gridAndTimeStepLength_forDC(  spaceStepLength, timeStepLength,  DC  );

  printf(  "Diffusion constant %g can be simulated space step %gμm and time step %gs\n",
           DC, spaceStepLength, timeStepLength  );
}
