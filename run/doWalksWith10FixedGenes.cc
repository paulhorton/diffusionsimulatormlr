/*  
 *  Author: Paul Horton
 *  Created: 20170217
 *  Updated: 20170608
 *
 */
#include "../ArgvParser_forSimulator.hh"
#include "../Simulator.hh"



int main( int argc, const char* argv[] ){
  ArgvParser_forSimulator
    argvParser(  "[--trace] [--ribosomes-help-anchor] [--dump-mito-hit-stats] freeDC trslDC freeAnchorProb trslAnchorProb numWalks",
                 argc, argv  );

  const bool  dumpTraceP            = argvParser.flagged( "--trace" );
  const bool  ribosomesHelpAnchorP  = argvParser.flagged( "--ribosomes-help-anchor" );
  const bool  dumpMitoHitStatsP     = argvParser.flagged( "--dump-mito-hit-stats" );

  const double freeDC          = argvParser.argVal<double>( 0 );
  const double trslDC          = argvParser.argVal<double>( 1 );
  const double freeAnchorProb  = argvParser.argVal<double>( 2 );
  const double trslAnchorProb  = argvParser.argVal<double>( 3 );
  
  const size_t numWalks        = argvParser.argVal<size_t>( 4 );

  argvParser.close();



  const doubleVecT avgTrslInitTimes   =  { 4.45,  8.56, 12.28, 18.57, 26.21, 37.84, 56.86, 84.06, 129.09, 381.51};
  const doubleVecT avgNumRibosomes    =  { 8.26,  8.50,  8.28,  5.50,  6.30,  4.20,  4.10,  1.82,   3.03,   2.02};
  const doubleVecT avgGivenMitoLocProb=  {0.087, 0.125, 0.182, 0.261, 0.289, 0.416, 0.419, 0.400,  0.469,  0.490};

  const MobilityParams  mobilityParams(  freeDC, trslDC,  freeAnchorProb, trslAnchorProb, ribosomesHelpAnchorP  );

  PointDiffusor pointDiffusor(  "fixed1"  );
  Simulator simulator(  pointDiffusor  );

  cout  <<  "translationInitTime\tnumRibosomes\tgivenMitoLocProb\tsimulatedMitoLocProb\n";

  for(  size_t i=  0;  i <  avgTrslInitTimes.size();  i++  ){
    const double trslInitTime=      avgTrslInitTimes   .at(i);
    const double numRibosomes=      avgNumRibosomes    .at(i);
    const double givenMitoLocProb=  avgGivenMitoLocProb.at(i);

    const Gene      gene     (   "Gene",  trslInitTime,  numRibosomes  );
    /***/ GeneStats geneStats(   "Gene"  );

    simulator.doWalks(  mobilityParams,  gene,  geneStats,  numWalks,  dumpTraceP  );
    const double simulMitoLocProb=   geneStats.numAnchorings() / (double) numWalks;

    cout  <<  trslInitTime     << '\t'
          <<  numRibosomes     << '\t'
          <<  givenMitoLocProb << '\t'
          <<  simulMitoLocProb << endl;

    if(  dumpMitoHitStatsP  )    geneStats.printStats();
  }
}
