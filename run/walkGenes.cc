/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.17
 *
 */
#include "../ArgvParser_forSimulator.hh"
#include "../Genes.hh"
#include "../Simulator.hh"


int main( int argc, const char* argv[] ){

  /*────────────────────  Read Command Line Arguments  ────────────────────*/
  ArgvParser_forSimulator argvParser(  "geneInfoFile freeDC trslDC freeAnchorProb trslAnchorProb numWalks varyTime|varyGrid baseLength",  argc, argv  );

  Genes genes(  argvParser(0)  );

  const double freeDC=         argvParser.argVal<double>(1);
  const double trslDC=         argvParser.argVal<double>(2);
  const double freeAnchorProb= argvParser.argVal<double>(3);
  const double trslAnchorProb= argvParser.argVal<double>(4);
  const size_t numWalks=       argvParser.argVal<size_t>(5);
  const string whatToVary=     argvParser.argVal<string>(6);
  const double baseLength=     argvParser.argVal<double>(7);

  argvParser.close();


  /*────────────────────  Set-up Simulation  ────────────────────*/
  printf(  "#freeDC=%g trslDC=%g freeAnchorProb=%g trslAnchorProb=%g  %s with baseLength=%g\n", freeDC, trslDC, freeAnchorProb, trslAnchorProb, whatToVary.c_str(), baseLength  );
  const MobilityParams mobilityParams(  freeDC, trslDC,  freeAnchorProb, trslAnchorProb  );

  PointDiffusor pointDiffusor(  "fixed1"  );
  Simulator simulator(  pointDiffusor  );

  genes.setNumWalks(  numWalks  );
  

  /*────────────────────  Run Simulation on each gene  ────────────────────*/
  cout  <<  "Gene\tgiven_MLP\tsim_MLP"  <<  endl;
  for(  size_t i= 0;  i < genes.size();  ++i  ){
    const Gene& gene=       genes.gene(i);
    GeneStats&  geneStats=  genes.geneStats(i);
    simulator.doWalks(  mobilityParams,  gene,  geneStats,  numWalks  );
    cout  <<  geneStats.ID()  <<  '\t'  <<  genes.given_mitoLocProb(i)  <<  '\t'  <<  geneStats.numAnchorings() / (double) numWalks  <<  endl;
  }
  cout  <<  "#given MLP vs. simulation MLP  Pearson corr:"  <<  genes.simulationCorr()  <<  "  MAE: "  <<  genes.simulationMAE()  <<  endl;
}

