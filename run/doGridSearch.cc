/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.17
 *
 */
#include "../ArgvParser_forSimulator.hh"
#include "../Genes.hh"
#include "../Simulator.hh"
#include "../EnumdParam.hh"
#include "../XableParam.hh"


int main( int argc, const char* argv[] ){

  /*────────────────────  Read Command Line Arguments  ────────────────────*/
  ArgvParser_forSimulator argvParser(  "geneInfoFile gridSearchInputFile numWalksPerSetting",  argc, argv  );

  Genes genes(  argvParser(0)  );

  const string gridSearchInputFilename=  argvParser(1);

  const size_t numWalksPerSetting=  argvParser.argVal<size_t>(2);
  genes.setNumWalks(  numWalksPerSetting  );

  argvParser.close();



  /*────────────────────  Open And Parse Grid Search Spec File  ────────────────────*/
  ifstream gridSearchInputStream(  gridSearchInputFilename  );
  if(  gridSearchInputStream.fail()  ){
    cerr  <<  "ERROR: when doing gridSearch could not open file '"  <<  gridSearchInputFilename
          <<  "' expected to hold params input.\n";
    exit(  64  );
  }

  cout  <<  "# Reading parameter search spec from file...\n";

  string line;

  // Skip comment lines.
  while(  getline(  gridSearchInputStream, line  )  ){
    if(  !line.size()    )   continue;
    if(   line[0] == ';' )   continue;
    break;
  }


  ASSERTF(  line.size(),
            "Did not find any non-comment lines in input file"  );
  XableParam freeDCs=  XableParam::make(  line  );

  ASSERTF(  freeDCs.name() == "freeDCs",
            "Expected first line to hold XableParam named freeDCs, but got one named %s\n.",  C(freeDCs.name())
            );
  cout  <<  "#  "  <<  line  <<  endl;


  DO_OR_DIE(  getline( gridSearchInputStream, line ),
              "Expected more lines after freeDC line"  );
  XableParam DC_downXs=  XableParam::make(  line  );

  ASSERTF(  DC_downXs.name() == "DC_downXs",
            "Expected first line to hold XableParam named DC_downXs, but got one named %s\n.",  C(DC_downXs.name())
            );

  cout  <<  "#  "  <<  line  <<  endl;


  DO_OR_DIE(  getline( gridSearchInputStream, line ),
              "Expected more lines after DC_downX line"  );
  EnumdParam freeAnchorProbs=  EnumdParam::make(  line  );

  ASSERTF(  freeAnchorProbs.name() == "freeAnchorProbs",
            "Expected first line to hold XableParam named freeAnchorProbs, but got one named %s\n.",  C(freeAnchorProbs.name())
            );

  freeAnchorProbs.assertAllValidProbs();
  cout  <<  "#  "  <<  line  <<  endl;


  DO_OR_DIE(  getline( gridSearchInputStream, line ),
              "Expected more lines after freeAnchorProbs line"  );
  EnumdParam trslAnchorProbs=  EnumdParam::make(  line  );

  ASSERTF(  trslAnchorProbs.name() == "trslAnchorProbs",
            "Expected first line to hold XableParam named trslAnchorProbs, but got one named %s\n.",  C(trslAnchorProbs.name())
            );

  trslAnchorProbs.assertAllValidProbs();
  cout  <<  "#  "  <<  line  <<  endl;
  cout  <<  "#  "            <<  endl;


  /*────────────────────  Conduct Grid Search and Output Results  ────────────────────*/
  printf(  "freeDC\ttrslDC\tfreeAnchorProb\ttrslAnchorProb\tsimMLP_mean\tsimMLP_Corr\tsimMLP_MAE\n"  );

  for(                  freeDCs.reset();          freeDCs.inRangeP();          freeDCs.next()   ){
    for(              DC_downXs.reset();        DC_downXs.inRangeP();        DC_downXs.next()   ){
      const double trslDC=   freeDCs() / DC_downXs();

      for(      freeAnchorProbs.reset();  freeAnchorProbs.inRangeP();  freeAnchorProbs.next()   ){
        for(    trslAnchorProbs.reset();  trslAnchorProbs.inRangeP();  trslAnchorProbs.next()   ){
          if(  trslAnchorProbs() < freeAnchorProbs()       )   continue;
          if(  freeAnchorProbs() + trslAnchorProbs() == 0  )   continue;

          MobilityParams mobilityParams(  freeDCs(), trslDC,  freeAnchorProbs(), trslAnchorProbs()  );

          PointDiffusor pointDiffusor(  "fixed1"  );
          
          Simulator simulator(  pointDiffusor  );

          for(  size_t i= 0;  i < genes.size();  ++i  ){
            simulator.doWalks(  mobilityParams,  genes.gene(i),  genes.geneStats(i),  numWalksPerSetting  );
          }

          const double simMLP_mean=  utils::mean(  genes.simulated_mitoLocProbs()  );
          const double simMLP_MAE =  genes.simulationMAE();
          const double simMLP_corr=  genes.simulationCorr();
          genes.zeroStats();
          printf(  "%g\t%g\t%g\t%g\t%g\t%g\t%g",
                   freeDCs(), trslDC, freeAnchorProbs(), trslAnchorProbs(), simMLP_mean, simMLP_corr, simMLP_MAE  );
          cout  <<  endl;   //Perhaps will help flush output...
        }
      }
    }
  }
}

