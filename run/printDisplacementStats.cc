/*  
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180322
 *  Updated: 20180322
 *
 *  Purpose: Reality check PointDiffusor displacement stats
 */
#include <iostream>
#include "../utils.hh"
#include "../ArgvParser_forSimulator.hh"
#include "../PointDiffusor.hh"



int main( int argc, const char* argv[] ){
  ArgvParser_forSimulator argvP(  "[options] diffusorSpec diffusionGranularity diffusionCoefficient numStepsPerTrial numTrials\noptions:  [-d|--dump] [-t|--tabular-summary]\n",
                                  argc, argv  );

  if(  argvP.flagged( "-h|--help" )  ){
    cout  <<  argvP.usage()
          <<  PointDiffusor::usage_diffusorSpec()          <<  endl
          <<  PointDiffusor::usage_granularity()           <<  endl
          <<  PointDiffusor::usage_diffusionCoefficient()  <<  endl;
    exit(1);
  }

  const bool dumpDisplacementsP=  argvP.flagged( "-d|--dump"            );
  const bool tabularSummaryP=     argvP.flagged( "-t|--tabular-summary" );

  const string diffuserSpec=     argvP(0);
  const double granularity=      argvP.argVal<double>(1);
  const double diffusionCoeff=   argvP.argVal<double>(2);
  const size_t numStepsPerTrial= argvP.argVal<double>(3);
  const size_t numTrials=        argvP.argVal<double>(4);
  argvP.close();

  ASSERTF(  granularity   > 0,    "expected positive value for granularity,   but got %g\n",  granularity      );
  ASSERTF(  diffusionCoeff > 0,   "expected positive value for diffusionCoeff,but got %g\n",  diffusionCoeff   );
  ASSERTF(  numStepsPerTrial > 0, "expected positive value for timePerTrial,  but got %zu\n", numStepsPerTrial );
  ASSERTF(  numTrials > 0,        "expected positive value for timePerTrial,  but got %zu\n", numTrials        );

  double walkTime;

  PointDiffusor pointDiffusor(  diffuserSpec, granularity  );
  pointDiffusor.setDiffusionCoeff(  diffusionCoeff  );

  if(  !tabularSummaryP  )
    printf(  "# timePerStep: %g,  MSDperStep: %g\n",  pointDiffusor.timePerStep(), pointDiffusor.MSDperStep()  );

  if(  dumpDisplacementsP  )    printf(  "Displacement\tx\ty\tz\n"  );


  vector<double> squaredDisplacements, X, Y, Z;

  for(  size_t curTrial= 0;  curTrial < numTrials;  ++curTrial  ){
    RetractablePoint point;
    pointDiffusor.resetTime();

    //  Diffuse for one trial...
    for(  size_t curStep= 0;  curStep < numStepsPerTrial;  ++curStep  ){
    pointDiffusor.moveOneStep( point );
  }

    const double displacement=  point.distanceToOrigin();
    squaredDisplacements.push_back(  displacement * displacement  );

    double x, y, z;   point.copyCoordsTo(  x, y, z  );
    X.push_back( x );  Y.push_back( y );  Z.push_back( z );

    walkTime=  pointDiffusor.elapsedTime();
    
    if(  !tabularSummaryP && curTrial==0  )   printf(  "# walk time per trial= %g\n", walkTime  );
    if(  dumpDisplacementsP  )                printf(  "%g\t%g\t%g\t%g\n",  displacement, x, y, z  );
  }

  
  const double MSD=    utils::mean ( squaredDisplacements );
  const double stDev=  utils::stDev( squaredDisplacements );
  const double empiricalD=  MSD / 6.0 / walkTime;
  const double meanX=  utils::mean( X );
  const double meanY=  utils::mean( Y );
  const double meanZ=  utils::mean( Z );

  const double raw_2momentX=  utils::rawMoment( 2, X );
  const double raw_2momentY=  utils::rawMoment( 2, Y );
  const double raw_2momentZ=  utils::rawMoment( 2, Z );
  const double raw_3momentX=  utils::rawMoment( 3, X );
  const double raw_3momentY=  utils::rawMoment( 3, Y );
  const double raw_3momentZ=  utils::rawMoment( 3, Z );


  if(  tabularSummaryP  ){
    cout <<  "DiffusorSpec"  <<  "\tMSD"      <<  "\tTime"     <<  "\tDisplacement"  <<  "\traw moments"  <<  "\t(x,y,z)"   <<  "\tMSD /\n"
         <<  ""              <<  "\tper step" <<  "\tper step" <<  "\t1st (means)"   <<  "\t2nd (MSDs)"   <<  "\t3rd"       <<  "\t 6 x simTime\n"
         <<  ""    <<  diffuserSpec
         <<  "\t"  <<  pointDiffusor.timePerStep()
         <<  "\t"  <<  pointDiffusor.MSDperStep()
         <<   std::setprecision(3)
         <<  "\t"  <<  meanX        <<  ','  <<   meanY         <<  ','  <<  meanZ
         <<  "\t"  <<  raw_2momentX <<  ','  <<   raw_2momentY  <<  ','  <<  raw_2momentZ
         <<  "\t"  <<  raw_3momentX <<  ','  <<   raw_3momentY  <<  ','  <<  raw_3momentZ
         <<  "\t"  <<  MSD / 6 / walkTime  <<  endl;
  }
  else{
    printf( 
           "mean displacement in (x,y,z)=  (%.3g,%.3g,%.3g)\n"
           "raw second moments in (x,y,z)= (%.3g,%.3g,%.3g)\n"
           "raw third  moments in (x,y,z)= (%.3g,%.3g,%.3g)\n"
           "Mean Squared Displacement:  %.4gμ ±%.4gμ\n"
           "Diffusion Coefficient estimated from squared displacement:  %.4gμ²/s\n",
           meanX, meanY, meanZ,
           raw_2momentX, raw_2momentY, raw_2momentZ,
           raw_3momentX, raw_3momentY, raw_3momentZ,
           MSD, stDev,
           empiricalD
            );
  }

  return 1;
}
