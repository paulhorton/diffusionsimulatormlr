/*  
    Author: Paul Horton
  
    Created: 20170217
 */
#include "../ArgvParser_forSimulator.hh"
#include "../Simulator.hh"



int main(  int argc,  const char* argv[]  ){
  ArgvParser_forSimulator
    argvParser(  "[--trace] freeDC trslDC freeAnchorProb trslAnchorProb avgTrslInitTime numWalks gridStepSize",
                 argc, argv  );

  const bool   dumpTraceP      = argvParser.flagged( "--trace" );

  const double freeDC          = argvParser.argVal<double>( 0 );
  const double trslDC          = argvParser.argVal<double>( 1 );
  const double freeAnchorProb  = argvParser.argVal<double>( 2 );
  const double trslAnchorProb  = argvParser.argVal<double>( 3 );
  
  const double avgTrslInitTime = argvParser.argVal<double>( 4 );
  const size_t numWalks        = argvParser.argVal<size_t>( 5 );
  const double gridStepSize    = argvParser.argVal<double>( 6 );

  argvParser.close();


  const MobilityParams  mobilityParams(  freeDC, trslDC,  freeAnchorProb, trslAnchorProb  );

  Gene      gene     (  "dummyGene",  avgTrslInitTime  );
  GeneStats geneStats(  "dummyGene"  );

  PointDiffusor pointDiffusor(  "fixed1"  );
  Simulator simulator(  pointDiffusor  );
  
  printf(  "strideDCs= (%g,%g),  anchorProbs= (%g,%g),  avgInitTime=%gs, num walks= %zu, gridStepSize= %g\n",
           freeDC, trslDC,  freeAnchorProb, trslAnchorProb,  avgTrslInitTime,  numWalks, gridStepSize   );

  simulator.doWalks(  mobilityParams,  gene,  geneStats,  numWalks,  dumpTraceP  );

  printf(  "MLR: %g\n",  geneStats.numAnchorings() / (double) numWalks  );
  geneStats.printStats();
}
