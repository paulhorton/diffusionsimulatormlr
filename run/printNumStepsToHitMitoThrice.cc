/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Creation Date: 20170217
 *  Last Modified: 20190326
 *
 *  Description: Print statistics on number of random walk steps before encountering mitochondria the 1st, 2nd and 3rd time.
 *               My expectation is that the distribution for the 2nd and 3rd times look the same (i.e. sampled from the same underlying distribution).
 *               
 *
 *  Purpose: Created for mRNA diffusion simulation project with Thomas Poulsen et al.
 *           This program was used to investigate the properties of the random walks.
 *
 */
#include "../ArgvParser_forSimulator.hh"
#include "../Simulator.hh"



int main( int argc, const char* argv[] ){
  ArgvParser_forSimulator argvParser(  "pointDiffusorSpec diffusionCoefficient(μm²/s) numRuns",  argc, argv  );

  const string diffusorSpec          = argvParser( 0 );
  const double diffusionCoefficient  = argvParser.argVal<double>( 1 );
  const size_t numRuns               = argvParser.argVal<size_t>( 2 );

  argvParser.close();


  PointDiffusor pointDiffusor( diffusorSpec );
  pointDiffusor.setDiffusionCoeff( diffusionCoefficient );

  Simulator simulator( pointDiffusor );

  cout  <<  "# time per step: "  <<  pointDiffusor.timePerStep()
        <<  " MSD per step: "    <<  pointDiffusor.timePerStep()  <<  endl;

  printf(  "numSteps1\tmito1\tnumSteps2\tmito2\tsame2\tnumSteps3\tmito3\tsame3\n"  );


  for(  size_t curRunNum= 0;  curRunNum < numRuns;  ++curRunNum  ){
    simulator.resetCytoplasm();

    vector<size_t> hitMitos;;
    vector<size_t> counts;

    for(  size_t hitMitoTimes= 0;  hitMitoTimes < 3;  ++hitMitoTimes  ){
      size_t  hitMito, count;
      for(  count= 0, hitMito= 0;
            !hitMito;
            count++  ){
        hitMito=  simulator.tryToStep_mRNA_hitoMitoP();
      }
      hitMitos.push_back( hitMito );
      counts.  push_back( count   );
    }

    const char* const sameP01=  (hitMitos[0] == hitMitos[1]?  "same" : "diff");
    const char* const sameP12=  (hitMitos[1] == hitMitos[2]?  "same" : "diff");

    printf(  "%zu\t%zu\t%zu\t%zu\t%s\t%zu\t%zu\t%s\n",
             counts[0], hitMitos[0],
             counts[1], hitMitos[1], sameP01,
             counts[2], hitMitos[2], sameP12  );

  }//numRuns

}
