/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.17
 *
 *  Description: Print statistics on average displacement of a particle after a random walk.
 *
 *  Purpose: Created for mRNA diffusion simulation project with Thomas Poulsen et al.
 *           Record number of hits to mitochondria within simulation to help explore the
 *           suitable range of anchor probabilities over a broad range of diffusion coefficients
 *           
 */
#include "../utils.hh"
#include "../Simulator.hh"
#include "../ArgvParser_forSimulator.hh"
#include "../XableParam.hh"



int main(  int argc,  const char* argv[]  ){
  ArgvParser_forSimulator
    argvParser(  "begDC endDC DCmultiplier numWalksPerDC",
                 argc, argv  );

  const double begDC          = argvParser.argVal<double>( 0 );
  const double endDC          = argvParser.argVal<double>( 1 );
  const double DCmultiplier   = argvParser.argVal<double>( 2 );
  const size_t numWalksPerDC  = argvParser.argVal<size_t>( 3 );

  argvParser.close();


  printf(  "# recordNumHits.  begDC=%gμ²/s, endDC=%gμ²/s, DCmultiplier=%g, numWalksPerDC=%zu\n",
           begDC, endDC, DCmultiplier, numWalksPerDC  );

  /*──────────  Print output column labels  ──────────*/
  cout  <<  "Diffusivity";
  for(  int i= 0;  i < 11;  i++  )    cout  <<  "\tT"  <<  i;
  cout  <<  endl;


  const Gene gene(  "forRecordingMitoHits",
              3   //  Dummy value for avgTrslInitTime 
              );

  GeneStats geneStats(  "forRecordingMitoHits"  );

  vector<size_t> numMitoHitsVec;

  for(   XableParam DC( "DC", begDC, endDC, DCmultiplier );   DC.inRangeP();   DC.next()    ){

    //  Set of mobility params that acts the same whether translating or not and never anchors.
    MobilityParams mobilityParams(  DC(), DC(),  0.0, 0.0  );

    PointDiffusor pointDiffusor(  "fixed1"  );
    Simulator simulator(  pointDiffusor  );

    numMitoHitsVec.clear();

    for(  size_t walkNum= 0;  walkNum < numWalksPerDC;  ++walkNum  ){
      simulator.doWalks(  mobilityParams,  gene,  geneStats  );
      numMitoHitsVec.push_back(  geneStats.numMitoHits()  );
      geneStats.zeroStats();
    }


    printf(  "%8.6f\t",  DC()  );
    cout  <<   utils::asString(  utils::deciles( numMitoHitsVec )  )   <<  endl;
  }//  next DC

}// main()
