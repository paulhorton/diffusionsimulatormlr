/*
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *  Description: See header file.
 */
#include "GeneStats.hh"


//  Initialize translation initiation probability, then call reset()
GeneStats::GeneStats(  const string& ID_  )
  :  _ID(ID_),
     _numAttemptsIdxMax(50)
{
  _numAnchorings_free.resize          (  1+ _numAttemptsIdxMax,  0  );
  _numAnchorings_trsl.resize          (  1+ _numAttemptsIdxMax,  0  );
  _numAnchorings_free_attempted.resize(  1+ _numAttemptsIdxMax,  0  );
  _numAnchorings_trsl_attempted.resize(  1+ _numAttemptsIdxMax,  0  );
}



void GeneStats::printStats() const{

  printf(  "Free anchor   on nth mito hito freq "  );
  for(  size_t const& num  :  _numAnchorings_free  ){
    printf(  " %2zu",  num  );
  }
  printf(  "\n"  );

  printf(  "Free attempts on nth mito hito freq "  );
  for(  size_t const& num  :  _numAnchorings_free_attempted  ){
    printf(  " %2zu",  num  );
  }
  printf(  "\n"  );

  printf(  "Trsl anchor   on nth mito hito freq "  );
  for(  size_t const& num  :  _numAnchorings_trsl  ){
    printf(  " %2zu",  num  );
  }
  printf(  "\n"  );

  printf(  "Trsl attempts on nth mito hito freq "  );
  for(  size_t const& num  :  _numAnchorings_trsl_attempted  ){
    printf(  " %2zu",  num  );
  }
  printf(  "\n"  );
}
