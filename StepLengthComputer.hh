/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  Description:
 *      Class to compute grid and time step lengths matching a given diffusion constant.
 *
 *  Purpose:  Part of reimplementation of a simulator conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"



class StepLengthComputer{
public:
  //  Set gridStepLength and timeStepLength to match diffusion constant DC.
  virtual void set_gridAndTimeStepLength_forDC(  double& gridStepLength,  double& timeStepLength,  const double DC  ) const  =0;

  static const StepLengthComputer *const newInstance(  size_t numDimsMovedPerStep, string whatToVary  );
  static const StepLengthComputer *const newInstance(  size_t numDimsMovedPerStep, string whatToVary,  double baseLength  );

protected:
  StepLengthComputer(  const size_t numDimsMovedPerStep  )
    : numDimsMovedPerStep_( numDimsMovedPerStep )
    {
      CHECKF(  numDimsMovedPerStep == 1 || numDimsMovedPerStep == 3,
               "StepLengthComputer constructor expected a value of 1 or 3 for numDimsMovedPerStep, but got %zu\n",
               numDimsMovedPerStep  );
    }

  const size_t numDimsMovedPerStep_;
};


class StepLengthComputer_varyTime : public StepLengthComputer{
public:
  StepLengthComputer_varyTime(  const size_t numDimsMovedPerStep,
                                const double gridStepLength=  0.001  )   // grid step length in microns
    : StepLengthComputer( numDimsMovedPerStep ),
      gridStepLength_(  gridStepLength  )
    {}

  void set_gridAndTimeStepLength_forDC(  double& gridStepLength,  double& timeStepLength,  const double DC  ) const{
    gridStepLength=  gridStepLength_;
    timeStepLength=  gridStepLength * gridStepLength * numDimsMovedPerStep_ / DC / 6.0;
  }

  const double gridStepLength_;
};


class StepLengthComputer_varyGrid : public StepLengthComputer{
public:
  StepLengthComputer_varyGrid(  const size_t numDimsMovedPerStep,                              
                                const double timeStepLength= 0.001  )   // time step length in secs
    : StepLengthComputer( numDimsMovedPerStep ),
      timeStepLength_(  timeStepLength  )
    {}

  void set_gridAndTimeStepLength_forDC(  double& gridStepLength,  double& timeStepLength,  const double DC  ) const{
    timeStepLength=  timeStepLength_;
    gridStepLength=  sqrt(  timeStepLength * DC * 6 / numDimsMovedPerStep_  );
  }

  const double timeStepLength_;
};
