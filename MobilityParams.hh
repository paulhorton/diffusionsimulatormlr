/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  Description: Class to hold data for parameters for a simulation of diffusion of mRNA molecules.
 *               In particular those parameters for which we plan to do optimization via parameter search.
 *
 *  Purpose:  Created as an extension of a simulator originally conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"
#include "utils.hh"


class MobilityParams{
public:
  
  /*───────────────  CONSTRUCTORS  ───────────────*/
  //  Construct from 4 strings.
  MobilityParams(  const string& freeDC_str,      const string& trslDC_str,
                   const string& freeAnchProbStr, const string& trslAnchProbStr,
                   bool ribosomesHelpAnchorP=  false
                   );

  MobilityParams(  const double freeDC_,       const double trslDC_,
                   const double freeAnchProb_, const double trslAnchProb_,
                   bool ribosomesHelpAnchorP=  false
                   );

  //  Reality check parameter values.
  void checkValidity() const;


  /*───────────────  ACCESSORS  ───────────────*/
  double freeAnchorProb() const{  return _freeAnchProb;  }
  double trslAnchorProb() const{  return _trslAnchProb;  }
  double freeDC()     const{  return _freeDC;    }
  double trslDC()     const{  return _trslDC;    }

  void set_freeDC( const double DC ){  _freeDC= DC;  }
  void set_trslDC( const double DC ){  _trslDC= DC;  }


  /*───────────────  OTHER METHODS  ───────────────*/
  double trslDC_adjusted(  const double numRibos  ) const{
  //  Someday may conditionally consider numRibos
    return _trslDC;
  }

  double trslAnchorProb_adjusted(  const double numRibos  ) const{
    return _anchorProbCalc( numRibos,_trslAnchProb );
  }
    
  
  /*───────────────  OBJECT DATA  ───────────────*/
private:
  double    _freeDC;   //   μm² / s 
  double    _trslDC;   //   μm² / s 
  double    _freeAnchProb;
  double    _trslAnchProb;
  
  double (*_anchorProbCalc)  (double, double);
};


inline  ostream& operator<<(  ostream& os,  const MobilityParams& params  ){
  os  <<  params.freeDC()  <<  ',' <<  params.trslDC()  <<  ':'  <<  params.freeAnchorProb()  <<  ','  <<  params.trslAnchorProb();
  return os;
}
