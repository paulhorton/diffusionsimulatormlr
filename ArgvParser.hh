/*  
  Author: Paul Horton
  Copyright (C) 2017, Paul Horton, All rights reserved.
  Created: 20170605   Based on an earlier design (~2005) by Paul Horton
  Updated: 20170607

  Description:  Skeleton of semi-general purpose class for handling command line arguments in a limited way.
                trimmed down to only handle what is needed for a particular application.

  Remarks:
    * Named options should all be parsed (using optVal) before parsing any positional arguments (using argVal)
    * Aliases are not currently supported (one way would be to take strings like "-v|--verbose" in lieu of optName).
    * Bundling is not supported (and probably not easy to add with this design).
    * Has been specilized as ArgvParser_forSimulator class

 */
#pragma once
#include <cxxabi.h>
#include "commonMacrosUsingTypedefs.hh"
#include "utils.hh"



class ArgvParser{
public:
  ArgvParser(  const string& _usage,
               const int argc, const char* argv[]
               );

  ~ArgvParser(){  close();  }

  size_t size() const{
    return args.size();
  }

  string usage() const;

  //  Return positional arg in position POS.
  template<typename T>
  T  argVal(  const size_t pos  ){
    argValCalledP=  true;
    const size_t argIdx=  nth_unused_asNamed_idx( pos );
    used[argIdx]=  positional;
    const string& arg=  args.at( argIdx );
    try{
      return    lexical_cast<T>(  arg  );
    }
    catch(  const bad_lexical_cast&  ){
      cout  <<  "Command Line Parsing Error; tried to parse  "  <<  arg  <<  " as "
            <<  pos  <<  "th positional argument, but could not convert it to "
            <<  typeStr(new T)  <<  " value.\n"
            <<  usageMsg;
      exit( 64 );
    }
  }


  //  If option named optName is present return its value,
  //  Otherwise return defaultVal.
  template<typename T>
  T  optVal(  const string& optName,  const T defaultVal  ){
    if(  argValCalledP  ){
      cout  <<  "Programming error: ArgvParser::optVal( '"  <<  optName  <<  "' ) called after ArgvParser::argVal().\n";
      exit( 1 );
    }

    const size_t valIdx=  idx_of_optVal( optName );
    if(  !valIdx  ){    return defaultVal;    }

    return _optVal<T>( valIdx, optName );
  }


  //  If (mandatory) option named optName is found, return its value.
  //  Otherwise abort.
  template<typename T>
  T  optVal(  const string& optName  ){
    if(  argValCalledP  ){
      cout  <<  "Programming error: ArgvParser::optVal( '"  <<  optName  <<  "' ) called after ArgvParser::argVal().\n";
      exit( 1 );
    }

    const size_t valIdx=  idx_of_optVal( optName );

    if(  !valIdx  ){
      cout  <<  "Command Line Parsing Error; could not find value of opt: '"  <<  optName  <<  "'\n"
            <<  usage();
      exit( 64 );
    }
    return _optVal<T>( valIdx, optName );
  }


  //  Returns true if any name in flagAliases appears on command line.
  bool flagged(  const stringDQ_T& flagAliases  );

  //  flagAliases should be a '|' separated list of one or more flagNames.
  inline
  bool flagged(  const string& flagAliases  ){
    return  flagged(  utils::splitOnPipes( flagAliases )  );
  }

  //  Returns true if any flagName appears on command line.
  bool nameFlagged(  const string& flagName  );


  //  Similar to flagVal(), except
  //  If the optName occurs at the very end (without a following value) flagVal( optName ) returns true, but hasOptVal( optName return false.
  //  hasOptVal does not mark the arguments holding the option name and value as used.
  bool hasOptVal(  const string& optName  )  const{
    return   0  <  idx_of_optVal( optName );
  }


  //  Called to indicate argv parsing should be finished.
  //  Exits with error if some args remain unused.  Otherwise does nothing.
  void close();

  
  //  Used for error messages.
  //  typeStr( new double ) returns  "double"
  //  typestr( new int )    return   "int"...
  template <typename T>  static
  const char* typeStr(const T* varOfType){
    return  abi::__cxa_demangle(typeid(*varOfType).name(), 0, 0, NULL);
  }

  //  Provided to allow derived classes to have default options.
  virtual  string defaultOptions()  const{   return  "";   }

  //  Marks returned argument as positionally used.
  string  operator()(  const size_t i  ){
    return   argVal<string>( i );
  }
private:

  //  Helper function of optVal.  Assumes valIdx is valid index of option value for option optName.
  template<typename T>
  T  _optVal(  const size_t valIdx, const string& optName  ){
    const string& arg=  args.at( valIdx );
    used[ valIdx-1 ]=  used[ valIdx ]=  named;
    try{
      return    lexical_cast<T>(  arg  );
    }
    catch(  const bad_lexical_cast&  ){
      cout  <<  "Command Line Parsing Error; tried to parse  "  <<  arg  <<  " as value of " <<  optName
            <<  " option, but could not convert it to a "
            <<  typeStr(new T)  <<  " value.\n"
            <<  usageMsg;
      exit( 64 );
    }
  }

  //  Return index of nth unused argument.
  //  idx counts from zero.
  size_t nth_unused_asNamed_idx(  size_t n  )  const;

  //  Return index of value of option preceded by optName.
  //  idx_of_optVal( "-r" ) should return 2 for command line like: progName 39 -r 29 fred...
  //  Returns zero if optName not found or is used.
  size_t idx_of_optVal(  const string& optName  )  const;

  
  /*───────────────  TYPEDEFS  -───────────────*/
  enum usedAsT{  never, positional, named  };
  
  /*───────────────  OBJECT DATA  -───────────────*/
protected:
  const string     usageSpec;  // Usage spec passed into constructor.
  const stringVecT argvCopy;   // To hold args as read
  
private:
  const stringDQ_T      args;
  /***/ deque<usedAsT>  used;   // Used as name or value of named opt
  /***/ bool            argValCalledP;
protected:
  const string     progName;
  /***/ string     usageMsg;
};
