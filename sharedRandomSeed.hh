/*
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180407
 *  Updated: 20180407
 *
 *  Shared random seed to allow simulation runs to be reproduced.
 *  used to seed srand, but also std::normal_distribution etc.
 */
#pragma once
#include <ctime>


void sharedRandomSeed_init(  time_t randomSeed= 0   );

time_t sharedRandomSeed();
