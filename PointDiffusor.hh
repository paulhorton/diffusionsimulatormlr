/*  
 *  Author: Paul Horton
 *  Creation Date: 20170317
 *
 *  Description: Abstact class to move a point one small step in 3D space
 *
 *  Purpose:  Created as part of diffusion simulation in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "RetractablePoint.hh"
#include "PointMover.hh"


class PointDiffusor{
public:
  PointDiffusor(  string diffusorSpec,
                  double granularity= 0.001  // time step length or MSD per step, depending on varyTime/Space part of spec
                  );

  void setDiffusionCoeff(  double DC  );

  double timePerStep() const{  return timePerStep_;  }

  double MSDperStep()  const{  return MSDperStep_;   }

  double elapsedTime() const{  return elapsedTime_;  }

  void resetTime(){  elapsedTime_= 0.0;  }

  void moveOneStep(  Point& point  ){
    ASSERTF(  DChasBeenSetP,
              "Internal Error: PointDiffusor::moveOneStep called before setting diffusion constant\n"  );
    elapsedTime_ += timePerStep_;
    pointMover_->move( point );
  }

  static string usage_diffusorSpec(){
    return  string(  "DiffusorSpec should match pattern:  /vary(dist|time):(gauss|fixed|fixed1)/"  );
  }

  static string usage_granularity(){
    return   string(  "Granularity is the variance of the distance in μm traveled per step (MSD) WHEN varying time, OTHERWISE it is the length of a time per step in seconds."  );
  }

  static string usage_diffusionCoefficient(){
    return   string(  "The diffusion coefficient should be given in units of μm^2/sec"  );
  }


private:
  bool        DChasBeenSetP=  false;
  double      elapsedTime_=   0.0;
  double      timePerStep_;
  double      MSDperStep_;
  bool        varyTimeP;     // when true time per step, instead of distance per step, is adjusted to obtain a desired difussion coefficient.
  PointMover* pointMover_;
};
