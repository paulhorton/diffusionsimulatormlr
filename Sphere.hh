/*  
 *  Author: Paul Horton
 *  Creation Date: 20170224
 *
 *  Description: Functions for a 3D random walk in a yeast cell.
 *
 *  Purpose:  Part of reimplementation of a simulator conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"
#include "utils.hh"
#include "Point.hh"


class Sphere{
public:
  /*───────────────  CONSTRUCTORS  ───────────────*/
  Sphere()
    :  _center( Point(0,0,0) ),  _radius(0)
  {}

  Sphere(  const Point& center_,  const double radius_  )
    :  _center(center_), _radius(radius_)
  {}


  /*───────────────  ACCESSORS ───────────────*/
  const Point& center()  const{  return _center;  }

  double radius()  const      {  return _radius;  }

  void setRadius(  const double radius_  ){
    _radius=  radius_;
  }


  /*───────────────  OTHER METHODS ───────────────*/

  //  Place randomly in spherical shell with inner radius r and outer radius R.
  void placeRandomly_withinOriginCenteredSphericalShell(  double r,  double R  );

  bool overlapsP(  const Point&  point  )  const{
    return                   _radius  >=   _center.distanceTo(  point  );
  }

  bool overlapsP(  const Sphere& sphere  )  const{
    return  sphere.radius() + radius()  >=   _center.distanceTo( sphere.center()  );
  }


  /*───────────────  OBJECT DATA  -───────────────*/
private:
  Point  _center;
  double _radius;
};


inline ostream& operator<<(  ostream& os, const Sphere& sphere  ){
  return
    os  <<   "center="   <<  sphere.center()  <<  " radius="  <<  sphere.radius();
}
