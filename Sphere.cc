//  This code is a rewrite by Paul Horton of an earlier program by Thomas Poulsen.
#include "Sphere.hh"



//  Place randomly in spherical shell bounded by inner radius r and outer radius R.
//  Implemented using rejection sampling.
void Sphere::placeRandomly_withinOriginCenteredSphericalShell(  const double r,  const double R  ){
  double q;

  //  First place randomly in the smallest cube containing the outer sphere of radius R.
  do{
    _center.moveToRandomPoint_inOriginCenteredCube( R );
    q=  _center.distanceToOrigin();
  }
  //  Redo until this sphere is enclosed in the spherical shell.
  while(    (r >  q - _radius)  ||
            (R <  q + _radius)    );
}


