# 3D random walk in a yeast cell simulator

This directory contains source code for a 3D random walk simulation of a molecule diffusing in a yeast cell.

This simulation software extends a similar simulation conceived of and initially implemented by Thomas Poulsen.


[Simulation Scheme](images/MLPsimulationScheme.png)


Borrowing from ideas in Thomas' initial implementation, the code in this repository was created from scratch with a modular design by Paul Horton.

[Software Structure](images/MLPsimulatorSoftwareStructure.png)


