/*  
 *  Author: Paul Horton
 *  Created: 20170227
 *  Updated: 20170607
 *
 *  Description: General utility functions
 *
 *  Purpose: Created for mRNA molecule diffusion simulation project
 *
 */
#pragma once
#include <sys/utsname.h>
#include <cmath>
#include <limits>
#include <boost/algorithm/string/predicate.hpp>
#include "commonMacrosUsingTypedefs.hh"


namespace utils{


  /*────────────────────  Simple Math Utilities  ────────────────────*/
  //  Return vector of deciles.  min and max are treated as 0th and 10th deciles.
  //  Sorts vec as side effect.
  vector<double> deciles(  vector<size_t>& vec  );


  template<typename T>
  double mean(  const std::vector<T>& vec  ){
    return   accumulate(  ALLOF(vec),  0.0  )  /  (double) vec.size();
  }


  template<typename T>
  double variance(  const vector<T>& vec  ){
    const double m= mean( vec );

    double SDM= 0.0;
    for(  const T& x :  vec  ){
      SDM +=  pow( x-m, 2 );
    }

    return  SDM  /  (double) (vec.size()-1);
  }

  template<typename T>
  double stDev(  const vector<T>& vec  ){
    return  sqrt( variance(vec));
  }


  //  Return Rth sample moment of numbers in VEC
  template<typename T>
  double rawMoment(  double r,
                     const vector<T>& vec
                     ){
    double sum=  0.0;

    for(  const T& num  :  vec  ){
      sum+=  pow(  num,  r  );
    }

    return   sum  /  (double) vec.size();
  }



  double meanAbsError(  const doubleVecT& vec1,  const doubleVecT& vec2  );


  double PearsonCorr(   const doubleVecT& vec1,  const doubleVecT& vec2  );



  inline size_t min(  const size_t a,  const size_t b  ){return   b < a ?  b : a   ;}


  inline void seedRandom(){
    const time_t ranSeed=  time(nullptr);
    srand( ranSeed );
  }

  inline void seedRandom(  const time_t ranSeed  ){
    srand( ranSeed );
  }

  
  /*────────────────────  Probability Related Utilities  ────────────────────*/
  inline bool happensP(  const double prob  ){
    if(  prob >= 1.0  ){   return true;   }
    return   prob >  (double) rand() / RAND_MAX;
  }                    

  inline bool inValidProbRangeP(  const double x  ){
    if(  0.0 <= x  &&  x <= 1.0  )   return 1;
    return 0;
  }


  //  Return probability of at least one success for $n Bernoulli trials with per-trial success probability $p,
  //  generalized to non-integer n.
  //  Does some parameter value checks and then returns  1 - (1-p)ⁿ
  double prob_atLeastOneSuccess(   double n,  double p   );


  inline void assertValidProb(  const double x,
                                const char*  msgPrefix  ){
    if(  inValidProbRangeP( x )  )   return;
    printf(  "%s invalid probability %g\n",  msgPrefix, x  );
    exit( 0 );
  }


  inline double randomNumber_fromRange(  const double rangeMin,  const double rangeMax  ){
    const double r=   (double)  rand() / RAND_MAX;
    return  rangeMin  +  r * (rangeMax - rangeMin);
  }



  /*────────────────────  Sundry Functions  ────────────────────*/
  inline double  justReturnArg2(  const double argToIgnore,  const double argToReturn  ){
    return  argToReturn;
  }

  // Return output of running shell cksum program on PATHNAME.
  string shell_cksum(  const string& pathname  );

  // Convert stringDQ to vector of doubles.  Die with msgOnErr if conversion to double fails.
  doubleVecT  toDoubleVec(  const string& msgOnErr,  const stringDQ_T& strings  );

  // Convert stringDQ to vector of doubles.  Die with msgOnErr if conversion to double fails.
  doubleVecT  toDoubleVec(  const string& msgOnErr,  const stringVecT& strings  );

  stringDQ_T  splitOnChar (  const char c,  const string& s  );
  inline stringDQ_T  splitOnColons(  const string& s  ){   return  splitOnChar(  ':',  s  );   }
  inline stringDQ_T  splitOnTabs  (  const string& s  ){   return  splitOnChar(  '\t', s  );   }
  inline stringDQ_T  splitOnPipes (  const string& s  ){   return  splitOnChar(  '|',  s  );   }

  //  If an element equal to valToDelete appears in strings, delete the first one and return true,
  //  otherwise do nothing and return false.
  bool deletedOneFromStringsP(  stringDQ_T& strings,  const string& valToDelete  );

  string  join(  const string& delim,  const stringDQ_T& strings  );

  bool any_eqciP(  const string& key,  const string& stringSet  );


  //  Return VEC as tab separated fields string.
  template<typename T>
  string  asString(  const vector<T>& vec  ){

    if(  !vec.size()  )   return  string("");

    std::ostringstream buffer;
    buffer  <<  vec.front();
    for(  auto itr= std::next( vec.begin() );  itr != vec.end();  ++itr  ){
        buffer  <<  '\t'  <<  *itr;
    }
    return  buffer.str();
  }


}//  namespace utils
