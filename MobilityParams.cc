/*
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *  Description: See header file.
 */
#include "MobilityParams.hh"


//  Construct from 4 strings.
MobilityParams::
MobilityParams(  const string& freeDC_str,   const string& trslDC_str,
                 const string& freeAnchProbStr, const string& trslAnchProbStr,
                 bool ribosomesHelpAnchorP
                 )
{
  try{
    _freeDC        =lexical_cast<double>( freeDC_str   );
    _trslDC        =lexical_cast<double>( trslDC_str   );
    _freeAnchProb  =lexical_cast<double>( freeAnchProbStr );
    _trslAnchProb  =lexical_cast<double>( trslAnchProbStr );
  }
  catch( const bad_lexical_cast& ){
    cout  <<  "When trying to construct MobilityParams, could not convert all of these four strings to double: "
          <<  "'"  <<  freeDC_str    <<  "',"
          <<  "'"  <<  trslDC_str    <<  "',"
          <<  "'"  <<  freeAnchProbStr  <<  "',"
          <<  "'"  <<  trslAnchProbStr  <<  "'\n";
    exit( 64 );
  }

  //  Function to calculate anchor probability.
  _anchorProbCalc= ribosomesHelpAnchorP?  utils::prob_atLeastOneSuccess  :  utils::justReturnArg2;

  checkValidity();
}



MobilityParams::
MobilityParams(  const double freeDC_,       const double trslDC_,
                 const double freeAnchProb_, const double trslAnchProb_,
                 bool ribosomesHelpAnchorP  
                 )
  : _freeDC(freeDC_), _trslDC(trslDC_), _freeAnchProb(freeAnchProb_), _trslAnchProb(trslAnchProb_)
{

  _anchorProbCalc= ribosomesHelpAnchorP?  utils::prob_atLeastOneSuccess  :  utils::justReturnArg2;
  checkValidity();
}



//  Reality check parameter values.
void MobilityParams::checkValidity() const{
  if(  _trslDC > _freeDC  ){
    printf(  "ERROR MobilityParams.trslDC(%g) exceeded freeDC(%g)\n", _trslDC, _freeDC  );
    exit( 64 );
  }

  utils::assertValidProb(  freeAnchorProb(),  "ERROR MobilityParams.freeAnchProb"  );
  utils::assertValidProb(  trslAnchorProb(),  "ERROR MobilityParams.trslAnchProb"  );

  if(  _freeAnchProb > _trslAnchProb  ){
    printf(  "ERROR MobilityParams.freeAnchorProb(%g) exceeded trslAnchorProb(%g)\n", _freeAnchProb, _trslAnchProb  );
    exit( 64 );
  }
}
