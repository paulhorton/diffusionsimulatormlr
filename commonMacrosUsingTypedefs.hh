/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.17
 *
 *  Description: Top of header file include for MLR simulation.
 *               Holds constants and other general header file content.
 *               
 */
#pragma once
#include <algorithm>
#include <cstdarg>  // for ASSERTF macro.
#include <cstdlib>
#include <cstdio>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>
#include <boost/lexical_cast.hpp>
using boost::bad_lexical_cast;
using boost::lexical_cast;
using std::cerr;
using std::cout;
using std::deque;
using std::endl;
using std::ifstream;
using std::istream;
using std::ostream;
using std::string;
using std::vector;

typedef  unsigned int    uint;
typedef  vector<double>  doubleVecT;
typedef  vector<string>  stringVecT;
typedef  deque<string>   stringDQ_T;


#define  ALLOF(X)  X.begin(), X.end()

#define  C(S)  (S.c_str())


#define DO_OR_DIE( exp, args... )			                                   \
  do{                                                                              \
    if( !(exp) ){							           \
      fprintf( stderr, "%s:%d Error; ", __FILE__, __LINE__ );	                   \
      fprintf( stderr, " assert true failed on the expression: \"%s\"\n", #exp );  \
      fprintf( stderr, args );                                                     \
      exit(-1);                                                                    \
    }                                                                              \
  } while(0)


#ifdef NDEBUG
#define ASSERTF( exp, args... )  /* noop */
#else
#define ASSERTF( exp, args... )			                                   \
  do{                                                                              \
    if( !(exp) ){							           \
      fprintf( stderr, "%s:%d Error; ", __FILE__, __LINE__ );	                   \
      fprintf( stderr, " assert true failed on the expression: \"%s\"\n", #exp );  \
      fprintf( stderr, args );                                                     \
      exit(-1);                                                                    \
    }                                                                              \
  } while(0)
#endif // NDEBUG defined or not


//  Like ASSERTF but always done (even if NDEBUG defined)
#define CHECKF( exp, args... )			                                   \
  do{                                                                              \
    if( !(exp) ){							           \
      fprintf( stderr, "%s:%d Error; ", __FILE__, __LINE__ );	                   \
      fprintf( stderr, " assert true failed on the expression: \"%s\"\n", #exp );  \
      fprintf( stderr, args );                                                     \
      exit(-1);                                                                    \
    }                                                                              \
  } while(0)




inline  ostream& operator<<(  ostream& os,  const doubleVecT& doubleVec  ){
  if(  doubleVec.size()  ){
    cout  <<  doubleVec[0];
    for(  size_t i= 1;  i < doubleVec.size();  ++i  )
      cout  <<  '\t'  <<  doubleVec[i];
    cout  <<  endl;
  }
  return os;
}
