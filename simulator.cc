/*
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *  Description: See header file.
 */
#include "simulator.hh"



namespace simulator{

  void doWalks(
               /***/ Cytoplasm&       cytoplasm,
               const MobilityParams&  mobilityParams,
               const Gene&            gene,
               /***/ GeneStats&       geneStats,
               const size_t           numWalks,
               const bool             dumpTraceP,
               const double           timeToWalkInSecs
               ){
    
    const size_t numStepsPerWalk=  (size_t)  (timeToWalkInSecs * numStepsPerSec());
    ASSERTF(  numStepsPerWalk > 0,  "Error in simulator, expected positive number of steps but got %zu", numStepsPerWalk  );


    for(  size_t curWalkNum= 0;  curWalkNum < numWalks;  ++curWalkNum   ){

      cytoplasm.resetForNewWalk();
      double anchorProb=  mobilityParams.freeAnchorProb();

      if(  dumpTraceP  )    cout  <<  "starting walk with anchorProb="  <<  anchorProb  <<  endl;

      bool translatingP=  false;
      size_t numAnchorAttempts=  0;

      for(  size_t curStepInWalk= 0;  curStepInWalk < numStepsPerWalk;  ++curStepInWalk  ){
        if(  dumpTraceP  ){
          cout  <<  curStepInWalk  <<  '\t'  <<  cytoplasm.mRNA_pos_asString()  <<  endl;
        }

        if(  !translatingP  &&  gene.initTranslationP()  ){
          translatingP=  true;
          double translatingDC=  mobilityParams.trslDC_adjusted(  gene.avgNumRibosomes()  );
          cytoplasm.set_mRNAstride(  strideLength_ofDiffusionCoefficient( translatingDC  )  );
          anchorProb=   mobilityParams.trslAnchorProb_adjusted(  gene.avgNumRibosomes()  );
          if(  dumpTraceP  )   cout  <<  "started translating and set anchorProb="  <<  anchorProb  <<  endl;
        }

        if(  cytoplasm.mRNAtryToStep_hitoMitoP()  ){
          geneStats.recordAnchoringAttempt( translatingP, numAnchorAttempts );
          if(   utils::happensP( anchorProb )   ){
            geneStats.recordAnchoring( translatingP, numAnchorAttempts );
            goto NEXT_WALK;
          }
          ++numAnchorAttempts;
        }

      }// curStepInWalk

    NEXT_WALK: ;
    }// numWalks

  }// doWalks


}// namespace simulator
