/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created: 20170607
 *  Updated: 20170607
 *
 *  Description: Extension of ArgvParser which handled parsing and logging of
 *               random seed option for a simulator application.
 *
 */
#pragma once
#include <sys/utsname.h>
#include "ArgvParser.hh"


class ArgvParser_forSimulator : public ArgvParser{
public:
  ArgvParser_forSimulator(  const string& usage,
                            const int argc, const char* argv[]
                            );

  string defaultOptions() const override{    return  "[-r ranSeed] ";    }
};

