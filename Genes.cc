/*
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *  Description: See header file.
 */
#include "Genes.hh"


Genes::Genes(  const string& inputPathname  ){

  ifstream inFStream(  inputPathname  );
  if(  inFStream.fail()  ){
    cerr  <<  "ERROR: could not open file '"  <<  inputPathname   <<  "' expected to hold data for Genes.\n";
    exit(  64  );
  }

  cout  <<  "# Reading Genes data from file with cksum: "  <<  utils::shell_cksum( inputPathname );

  string line;
  getline(  inFStream, line  );

  const string header= "Gene\tavgTranslationInitTimeSecs\tavgNumRibosomes\tgivenMitoLocProb";
  if(  line != header  ){
    cerr  <<  "Error while constructing MLR_Genes: Expected line of geneInfo file to be:\n"  << header  <<  "\nbut the line is:\n"  <<  line  <<  endl;
    exit( 64 );
  }

  stringDQ_T fields=  utils::splitOnTabs( line );
  if(  fields.size() != 4  ){
    cerr  <<  "Error while constructing MLR_Genes: Expected first line to have 4 tab separated fields but the line is:  "  <<  line  <<  endl;
    exit( 64 );
  }

  for(  size_t lineNum= 2;  getline( inFStream, line );  ++lineNum  ){
    fields=  utils::splitOnTabs( line );
    if(  fields.size() < 4  ){
      fprintf(  stderr, "Error while constructing MLR_Genes: Expected first %zu line to have 4 tabs separated fields but the line is: %s\n",  lineNum,  C(line)  );
      exit( 64 );
    }

    if(  !fields[2].size()  )    fields[2]=  "1.0";  // default value for numRibos.

    double avgTrslInitTime, numRibosomes, given_mitoLocProb;
    try{
      avgTrslInitTime   =lexical_cast<double>(  fields.at(1)  );
      numRibosomes      =lexical_cast<double>(  fields.at(2)  );
      given_mitoLocProb =lexical_cast<double>(  fields.at(3)  );

    }
    catch(  const bad_lexical_cast&   ){
      fprintf(  stderr, "Error while constructing MLR_Genes: On input line %zu, expected fields 1–3 to contain real numbers but they contain '%s', '%s', '%s'\n",  lineNum,  C( fields[1] ), C( fields[2] ),  C( fields[3] )   );
      exit( 64 );
    }

    _geneVec.push_back(     Gene(   fields[0],  avgTrslInitTime,  numRibosomes   ));
    _given_mitoLocProbs.push_back(  given_mitoLocProb  );
    _geneStatsVec.push_back(  GeneStats( fields[0] )  );
  }//for line


  _numWalks= 0;
}//  Genes::Genes



ostream& operator<<(  ostream& os,  const Genes& genes  ){
  os  <<  "Gene"  <<  '\t' <<  "avgTrslInitTime"  <<  '\t' <<  "avgNumRibosomes"  <<  '\t' <<  "givenMitoLocProb"  <<  endl;
  for(  size_t i= 0;  i < genes.size();  i++  ){
    const Gene& gene=  genes._geneVec.at(i);
    os  <<  gene.ID()  <<  '\t' <<  gene.avgTrslInitTime()  <<  '\t' << gene.avgNumRibosomes()
        <<  '\t' <<  genes._given_mitoLocProbs.at(i)  <<  endl;
  }
  return os;
}
