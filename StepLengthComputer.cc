/*
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created: 20171128
 *  Updated: 20171128
 *  Description: See header file.
 */
#include "StepLengthComputer.hh"


const StepLengthComputer *const
StepLengthComputer::newInstance(  const size_t numDimsMovedPerStep,
                                  const string whatToVary  ){
  if(  whatToVary == "varyGrid"  ){
    return  new StepLengthComputer_varyGrid( numDimsMovedPerStep );
  }
  if(  whatToVary == "varyTime"  ){
    return  new StepLengthComputer_varyTime( numDimsMovedPerStep );
  }
  cerr  <<   "Expected varyGrid|varyTime, but got '"  <<  whatToVary  <<  "'\n";
  exit( -1 );
}



const StepLengthComputer *const
StepLengthComputer::newInstance(  const size_t numDimsMovedPerStep,
                                  const string whatToVary,  const double baseLength  ){
  if(  whatToVary == "varyGrid"  ){
    return  new StepLengthComputer_varyGrid( numDimsMovedPerStep, baseLength );
  }
  if(  whatToVary == "varyTime"  ){
    return  new StepLengthComputer_varyTime( numDimsMovedPerStep, baseLength );
  }
  cerr  <<   "Expected varyGrid|varyTime, but got '"  <<  whatToVary  <<  "'\n";
  exit( -1 );
}
