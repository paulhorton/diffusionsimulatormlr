//  This code is a rewrite by Paul Horton of an earlier program by Thomas Poulsen.
#include "utils.hh"
#include "DiffusingPoint.hh"


void DiffusingPoint::walkOneRandomStep(){
  ASSERTF(  stride > 0,  "Invalid string value (%g), DiffusingPoint probably was not initialized properly.\n", stride  );
#ifndef NDEBUG
  prevCalledMethod=  methodName::walkOneRandomStep;
#endif

  prevX= x;  prevY= y;  prevZ= z;

  const double curRandomNumber=  utils::randomNumber_fromRange(  0.0, 6.0  );

  if(  curRandomNumber < 3  ){
    /**/ if( curRandomNumber < 1 )    x += stride;
    else if( curRandomNumber < 2 )    x -= stride;
    else                              y += stride;
  }
  else{
    /**/ if( curRandomNumber < 4 )    y -= stride;
    else if( curRandomNumber < 5 )    z += stride;
    else                              z -= stride;
  }
}


void DiffusingPoint::undoPreviousStep(){
  ASSERTF(  prevCalledMethod == methodName::walkOneRandomStep,
            "expected prevCalledMethod to be 'walkOneRandomStep', but it was '%s'\n.", methodAsCStr( prevCalledMethod )
            );
  x= prevX;  y= prevY;  z= prevZ;
}

