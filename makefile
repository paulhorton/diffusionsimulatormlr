# Author Paul Horton, 2017.
#
.PHONY: all clean run_files


CXXDEBUG?= -g   # To overide asserts use:  make CXXDEBUG=-DNDEBUG
CXXOPT?=   -O3
CXXWARN?=  -Wall -Wshadow -Wmissing-declarations -Wwrite-strings -Wcast-align -Wcast-qual -Wpointer-arith -Werror
CXXFLAGS:= $(CXXDEBUG) $(CXXOPT) $(CXXWARN) -std=c++11


#MOD short for module, e.g. a C++ class.
MOD_CCS:=      $(wildcard *.cc)
MOD_OBJS_AUX:= $(MOD_CCS:.cc=.o)
MOD_OBJS:=     $(patsubst %,obj/%,$(MOD_OBJS_AUX))
MOD_DEPS_AUX:= $(MOD_OBJS:.o=.dep)
MOD_DEPS:=     $(patsubst obj/%,dep/%,$(MOD_DEPS_AUX))

TRY_SRCS:=     $(wildcard try/*.cc)
TRY_BINS_AUX:= $(TRY_SRCS:.cc=)
TRY_BINS:=     $(patsubst try/%,bin/%,$(TRY_BINS_AUX))
TRY_DEPS_AUX:= $(TRY_SRCS:.cc=.dep)
TRY_DEPS:=     $(patsubst try/%,dep/%,$(TRY_DEPS_AUX))

RUN_SRCS:=     $(wildcard run/*.cc)
RUN_BINS_AUX:= $(RUN_SRCS:.cc=)
RUN_BINS:=     $(patsubst run/%,bin/%,$(RUN_BINS_AUX))
RUN_DEPS_AUX:= $(RUN_SRCS:.cc=.dep)
RUN_DEPS:=     $(patsubst run/%,dep/%,$(RUN_DEPS_AUX))


BINS:=  $(TRY_BINS) $(RUN_BINS)
DEPS:=  $(MOD_DEPS) $(TRY_DEPS) $(RUN_DEPS)


all: $(BINS)

run_files: $(RUN_BINS)

$(MOD_OBJS) : obj/%.o : %.cc dep/%.dep
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(MOD_DEPS) : dep/%.dep : %.cc
	@mkdir -p $(@D)
	$(CXX) -MTobj/$*.o -MM -MF $@ $(CXXFLAGS) $^

$(TRY_DEPS) : dep/%.dep : try/%.cc
	@mkdir -p $(@D)
	./coldeps $< > $@

$(RUN_DEPS) : dep/%.dep : run/%.cc
	@mkdir -p $(@D)
	./coldeps $< > $@



clean:
	@rm -f $(filter-out %README,$(wildcard bin/* dep/* obj/*))

ifneq (clean,$(filter clean,$(MAKECMDGOALS)))
 -include $(DEPS)
endif 
