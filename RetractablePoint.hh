/*  
 *  Author: Paul Horton
 *  Creation Date: 20170224
 *
 *  Description: Extention of Point which remembers its previous position
 *
 */
#pragma once
#include "Point.hh"



class RetractablePoint : public Point{
public:
  RetractablePoint()
    : Point()
  {
    prevX= x;  prevY= y;  prevZ= z;
  }

  void moveTo(  double xNew, double yNew, double zNew  ){
    prevX= x;  prevY= y;  prevZ= z;
    Point::moveTo(  xNew, yNew, zNew  );
  }

  void moveTo( const Point& point ){
    prevX= x;  prevY= y;  prevZ= z;
    Point::moveTo(  point  );
  }

  void retractPrevMove(){
    x= prevX;  y= prevY;  z= prevZ;
  }

  friend ostream& operator<<(  ostream& os, const RetractablePoint& rpoint  );

private:
  static ostream& numberFormat( ostream& os ){
    return
      os  <<  std::setprecision(4)  <<  std::showpos  <<  std::setw(7)  <<  std::left  <<  std::fixed;
  }

  double prevX, prevY, prevZ;
};


inline ostream& operator<<(  ostream& os, const RetractablePoint& rpoint  ){
  return
    os  <<  rpoint.numberFormat  <<  rpoint.x  <<  ';'
        <<  rpoint.numberFormat  <<  rpoint.y  <<  ';'
        <<  rpoint.numberFormat  <<  rpoint.z
        <<  "  prev:"
        <<  rpoint.numberFormat  <<  rpoint.prevX  <<  ';'
        <<  rpoint.numberFormat  <<  rpoint.prevY  <<  ';'
        <<  rpoint.numberFormat  <<  rpoint.prevZ;
}
