/*
 *  Author: Paul Horton
 *  Created 20171125 from previous simulator.cc
 *  Description: See header file.
 */
#include "Simulator.hh"



//  Tentatively make mRNA take one random step.
//  If that would not hit anything, accept the step and return 0.
//  Otherwise, undo the step and: if the thing hit was a mitochondria return its number (≧1), else return 0.
size_t Simulator::tryToStep_mRNA_hitoMitoP(){

  pointDiffusor.moveOneStep(  cytoplasm.mRNA  );

  size_t mitoNum=  cytoplasm.mRNAinMitoP();
  if(   mitoNum  ||  !cytoplasm.mRNAinCytoplasmP()   ){
    cytoplasm.mRNA.retractPrevMove();
  }

  return mitoNum;
}



void Simulator::doWalks(
               const MobilityParams&  mobilityParams,
               const Gene&            gene,
               /***/ GeneStats&       geneStats,
               const size_t           numWalks,
               const bool             dumpTraceP,
               const double           timeToWalkInSecs
               ){

  ASSERTF(  timeToWalkInSecs > 0,  "Error in simulator, expected positive amount of time to walk but got %g", timeToWalkInSecs  );


  for(  size_t curWalkNum= 0;  curWalkNum < numWalks;  ++curWalkNum   ){

    cytoplasm.resetForNewWalk();
    double anchorProb=  mobilityParams.freeAnchorProb();

    if(  dumpTraceP  )    cout  <<  "starting walk with anchorProb="  <<  anchorProb  <<  endl;

    pointDiffusor.setDiffusionCoeff(  mobilityParams.freeDC()  );

    size_t numAnchorAttempts=  0;
    bool translatingP=  false;

    pointDiffusor.resetTime();
    while(  pointDiffusor.elapsedTime() < timeToWalkInSecs  ){
      if(  dumpTraceP  ){
        cout  <<  pointDiffusor.elapsedTime()  <<  '\t'  <<  cytoplasm.mRNA.asString()  <<  endl;
      }

      if(   !translatingP  &&
            gene.initTranslationP( pointDiffusor.timePerStep() )
            ){
        translatingP=  true;
        anchorProb=   mobilityParams.trslAnchorProb_adjusted(  gene.avgNumRibosomes()  );
        pointDiffusor.setDiffusionCoeff(  mobilityParams.trslDC_adjusted(  gene.avgNumRibosomes())  );
        if(  dumpTraceP  )   cout  <<  "started translating and set anchorProb="  <<  anchorProb  <<  endl;
      }

      if(  tryToStep_mRNA_hitoMitoP()  ){
        geneStats.recordAnchoringAttempt( translatingP, numAnchorAttempts );
        if(   utils::happensP( anchorProb )   ){
          geneStats.recordAnchoring( translatingP, numAnchorAttempts );
          goto NEXT_WALK;
        }
        ++numAnchorAttempts;
      }

    }// while pointDiffusor.elapsedTime() < timeToWalkInSecs  )

  NEXT_WALK: ;
  }// numWalks

}//void Simulator::doWalks
