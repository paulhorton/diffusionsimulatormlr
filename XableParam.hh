/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.3.20
 *
 *  Description: Class to hold information about parameter which can be searched over by multiplying by a number in (0.0,1.0].
 *
 *  Purpose: Created as reimplementation of a random walk simulation for mRNA molecules originally conceived of and written by Thomas Poulsen.
 */
#pragma once
#include "utils.hh"


class XableParam{
public:

  /*───────────────  CONSTRUCTORS  ───────────────*/
  static XableParam make(  const string initData  );

  XableParam(  const string& name_,  double initVal,  double minVal,  double multiplier= 0.0  )
    :
    _name( name_ ), curVal( initVal ), _initVal(initVal), _minVal(minVal), _multiplier(multiplier)
  {
    ASSERTF(  initVal >= 0,
              "When constructing XableParam %s; expected init value to be non-negative but got %g\n",  C(name_), initVal
              );
    ASSERTF(  minVal >= 0,
              "When constructing XableParam %s; expected min value to be non-negative but got %g\n",  C(name_), minVal
              );
    ASSERTF(  initVal >= minVal,
              "When constructing XableParam %s; expected init value(%g) ≧ min value(%g)\n",  C(name_), initVal, minVal
              );
    ASSERTF(  utils::inValidProbRangeP( multiplier ),
              "When constructing XableParam %s; expected multiplier value to be in (0.0,1.0] but got %g\n",
              C(name_), multiplier
              );
  }


  /*───────────────  ACCESSORS  ───────────────*/
  const string& name() const{  return _name;  }

  double operator()() const{  return curVal;  }

  void reset(){  curVal= _initVal;  }

  bool inRangeP(){   return curVal >= _minVal;   }

  friend ostream& operator<<(ostream&, const XableParam&);


  /*───────────────  OTHER METHODS  ───────────────*/
  double next(){   return(  curVal*= _multiplier  );   }



  /*───────────────  OBJECT DATA  ───────────────*/
private:
  const string  _name;
  /* */ double  curVal;
  const double  _initVal;
  const double  _minVal;
  const double  _multiplier;
};


inline  ostream& operator<<(  ostream& os,  const XableParam& param  ){
  return   os  <<  param.name()  <<   ':' << param._initVal  <<  ':' << param._minVal  << ':' << param._multiplier;
}
