/*
 *  Author: Paul Horton
 *  Creation Date: 2017.3.22
 *  Description: See header file.
 */
#include "XableParam.hh"



XableParam XableParam::make(  const string initData  ){
  stringDQ_T fields=  utils::splitOnTabs(  initData  );

  ASSERTF(  fields.size() > 1,
            "When constructing EnumdParam; passed string '%s'\nwith less than two tab separated fields\n",  C(initData)
            );

  const string name=  fields[0];
  fields.pop_front();

  ASSERTF(  fields.size() == 3,
            "When constructing XableParam %s; expected name to be followed by 3 first, but it was followed by %zu fields.",
            C(name), fields.size()
            );

  const doubleVecT vals=  utils::toDoubleVec(  "When constructing EnumParam " + name,  fields  );

  return  XableParam(  name, vals[0], vals[1], vals[2]  );
}
