#include <cstdlib>
#include "sharedRandomSeed.hh"


// shared random seed to allow simulation runs to be reproduced.
// used to seed srand, but also std::normal_distribution etc.
static time_t seed;

void sharedRandomSeed_init(  time_t randomSeed  ){
  seed=  randomSeed;
  if( !seed)   seed=  time(NULL);
  srand( seed );
}

time_t sharedRandomSeed(){  return seed;  }

