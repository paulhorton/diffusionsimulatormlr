/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  Description: Class to hold information about a gene.
 *
 *  Purpose: Created as an extension of a random walk simulation for mRNA molecules conceived of by Thomas Poulsen.
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"
#include "utils.hh"



class Gene{
public:

  /*───────────────  CONSTRUCTORS  ───────────────*/
  Gene(  const string& ID_,  const double avgTrslInitTime_,  const double avgNumRibosomes_= 1.0  )
    :  _ID(ID_),
       _avgTrslInitTime( avgTrslInitTime_ ),
       _trslInitProb_perSec(  1.0 / _avgTrslInitTime  ),
       _avgNumRibosomes( avgNumRibosomes_ )
  {
    utils::assertValidProb(  _trslInitProb_perSec,
                             "ERROR Gene trsl initiation probability per sec initialized with"  );
  }


  //  Initialize translation initiation probability and statistic keeping arrays.
  void initialize(  const double avgTrslInitTime  );


  /*───────────────  ACCESSORS  ───────────────*/
  const string&  ID() const{  return _ID;  }
  double  avgTrslInitTime() const{   return _avgTrslInitTime;   }
  double  avgNumRibosomes() const{   return _avgNumRibosomes;   }


  /*───────────────  OTHER METHODS  ───────────────*/
  bool initTranslationP(  const double timeStepInSecs  ) const{
    return   utils::happensP(  timeStepInSecs *_trslInitProb_perSec  );
  }


  /*───────────────  OBJECT DATA  ───────────────*/

private:
  const string  _ID;
  const double  _avgTrslInitTime;
  const double  _trslInitProb_perSec;
  const double  _avgNumRibosomes;
};


inline  ostream& operator<<(  ostream& os,  const Gene& gene  ){
  return
    os  <<  "ID:"  <<  gene.ID()
        <<  " avg(trslProb,numRibos)= ("  <<  gene.avgTrslInitTime()  <<  ','  <<  gene.avgNumRibosomes()  <<  ')';
}
