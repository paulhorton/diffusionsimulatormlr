/*  
 *  Author: Paul Horton
 *  Created: 20170224
 *
 *  Description: Molecular Diffusion Simulation.
 *
 *  Purpose:  Part of an extention of a simulator conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "Cytoplasm.hh"
#include "Gene.hh"
#include "GeneStats.hh"
#include "MobilityParams.hh"
#include "StepLengthComputer.hh"


class Simulator{
public:

  Simulator(  PointDiffusor& pointDiffusor_  )
    : pointDiffusor( pointDiffusor_ )
      {}

  void resetCytoplasm(){  cytoplasm.resetForNewWalk();  }

  //  Tentatively make mRNA take one random step.
  //  If that would not hit anything, accept the step and return 0.
  //  Otherwise, undo the step and: if the thing hit was a mitochondria return its number (≧1), else return 0.
  size_t tryToStep_mRNA_hitoMitoP();


  void doWalks(
               const MobilityParams&  mobilityParams,
               const Gene&            gene,
               /***/ GeneStats&       geneStats,
               const size_t           numWalks=    1,
               const bool             dumpTraceP=  false,
               const double           timeToWalkInSecs=  660   // 11 minutes, average half-life of yeast mRNA.
               );

private:
  PointDiffusor&  pointDiffusor;
  Cytoplasm       cytoplasm;
};
