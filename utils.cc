#include "utils.hh"


namespace utils{


vector<double> deciles(  vector<size_t>& vec  ){

  ASSERTF(  vec.size() > 1,
            "Error: deciles called vector of size %zu which is < 2.  Aborting...\n",  vec.size()
            );

  vector<double> retVal;

  std::sort(  ALLOF(vec)  );

  const double n=  (double) vec.size();

  retVal.push_back(  (double) vec[0]  );

  for(  double d= 1;  d < 9.5;  ++d  ){
    double quotient=  d*n / 10;
    int dw=  (int) floor( quotient );
    int up=  (int)  ceil( quotient );
    double  r=  quotient - dw;
    ASSERTF(  inValidProbRangeP( r ),
              "Bad value for r(%g)", r  );
    retVal.push_back(  (1-r) * (double) vec.at(dw)  +  r * (double) vec.at(up)  );
  }

  retVal.push_back(  (double) vec.back()  );

  return retVal;
}// deciles( vec )



double meanAbsError(  const doubleVecT& vec1,  const doubleVecT& vec2  ){
  ASSERTF(  vec1.size() == vec2.size(),
            "meanAbsError vec1 size(%zu) ≠ vec2 size(%zu)", vec1.size(), vec2.size()  );
  double errorSum=  0.0;
  for(  size_t i= 0;  i < vec1.size();  ++i  ){
    errorSum+=   fabs(  vec1[i] - vec2[i]  );
  }
  return  errorSum / (double) vec1.size();
}


double PearsonCorr(   const doubleVecT& vec1,  const doubleVecT& vec2  ){
  ASSERTF(  vec1.size() == vec2.size(),
            "PearsonCorr vec1 size(%zu) ≠ vec2 size(%zu)", vec1.size(), vec2.size()  );

  ASSERTF(  vec1.size() > 1,
            "PearsonCorr called with vectors of size (%zu), but requires vectors with at least two elements", vec1.size()  );

  const double mean1=   mean( vec1 );
  const double mean2=   mean( vec2 );
  const double stDev1= stDev( vec1 ); 
  const double stDev2= stDev( vec2 );

  if(  stDev1 == 0  )    return  std::numeric_limits<double>::quiet_NaN();
  if(  stDev2 == 0  )    return  std::numeric_limits<double>::quiet_NaN();

  double standardScoreProd_sum=  0.0;
  for(  size_t i= 0;  i < vec1.size();  ++i  ){
    standardScoreProd_sum+=  (vec1[i]-mean1) * (vec2[i]-mean2) / stDev1 / stDev2;
  }

  return  standardScoreProd_sum  /  (double) (vec1.size()-1);

}


// Return output of running shell cksum program on PATHNAME.
string shell_cksum(  const string& pathname  ){
  string command=  "cksum " + pathname;

  FILE* inputFromCksum=    popen(   C(command),  "r"   );

  ASSERTF(  inputFromCksum,
            "Failed to run cksum on pathname %s", C(pathname)  );
  char buff[512];

  ASSERTF(  fgets(buff, sizeof(buff), inputFromCksum),
            "Failed to fetch results of running cksum on pathname %s", C(pathname)  );

  return  string(buff);
}


// Convert stringDQ to vector of doubles.  Die with msgOnErr if conversion to double fails.
doubleVecT  toDoubleVec(  const string& msgOnErr,  const stringDQ_T&  strings  ){
  doubleVecT retVal;
  for(  size_t i= 0;  i < strings.size();  ++i  ){
    try{
      double val=  lexical_cast<double>(  strings[i]  );
      retVal.push_back( val );
    }
    catch( const bad_lexical_cast& ){
      printf(  "%s Could not convert %zuth element '%s' to a double",  C(msgOnErr),  i,  C( strings[i])   );
      exit(  64  );
    }
  }
  return retVal;
}  


// Convert stringVect to vector of doubles.  Die with msgOnErr if conversion to double fails.
// Convert stringDQ to vector of doubles.  Die with msgOnErr if conversion to double fails.
doubleVecT  toDoubleVec(  const string& msgOnErr,  const stringVecT&  strings  ){
  doubleVecT retVal;
  for(  size_t i= 0;  i < strings.size();  ++i  ){
    try{
      double val=  lexical_cast<double>(  strings[i]  );
      retVal.push_back( val );
    }
    catch( const bad_lexical_cast& ){
      printf(  "%s Could not convert %zuth element '%s' to a double",  C(msgOnErr),  i,  C( strings[i])   );
      exit(  64  );
    }
  }
  return retVal;
}  


  //  Return probability of at least one success for $n Bernoulli trials with per-trial success probability $p,
  //  generalized to non-integer n.
  //  Does some parameter value checks and then returns  1 - (1-p)ⁿ
  double prob_atLeastOneSuccess(  const double n,  const double p  ){
    ASSERTF(  inValidProbRangeP( p ),
              "prob_atLeastOneSuccess called with invalid probability (%g)", p
              );
    ASSERTF(  n > 0,
              "prob_atLeastOneSuccess called with non-positive number of trials (%g)", n
              );
    const double retVal=    1  -   pow(  1-p, n  );
    ASSERTF(  inValidProbRangeP( retVal ),
              "prob_atLeastOneSuccess computation on (n=%g,p=%g) yielded invalid probability %g\n", n, p, retVal
              );
    return  retVal;
  }


  // Split string s on char c.
  stringDQ_T  splitOnChar(  const char c, const string& s ){
    stringDQ_T retVal;
    size_t curFieldBeg= 0;
    for(  size_t i= 0;  i < s.size();  i++  ){
      if(  s[i] == c  ){
        retVal.push_back(   s.substr( curFieldBeg, i-curFieldBeg )   );
        curFieldBeg=  i+1;
      }
    }
    retVal.push_back(   s.substr( curFieldBeg, s.size()-curFieldBeg )   );

    return retVal;
  }


  bool  deletedOneFromStringsP(  stringDQ_T& strings,  const string& valToDelete  ){
    auto valI=  find(  ALLOF(strings), valToDelete  );
    if(  valI == strings.end()  )    return false;

    strings.erase( valI );   return true;
  }


  string  join(  const string& delim, const stringDQ_T& strings  ){

    if(  !strings.size()  )    return string("");
    string retVal(  strings[0]  );
  
    for(  size_t i= 1;  i < strings.size();  i++  ){
      retVal+=  delim + strings[i];
    }
    return retVal;
  }


  bool any_eqciP(  const string& key,  const string& stringSet  ){
    stringDQ_T  elements=  splitOnPipes( stringSet );

    for(  auto s  :  elements  ){
      if(  boost::iequals( key, s )  ){   return true;   }
    }
    return false;
  }


}//  namespace utils
