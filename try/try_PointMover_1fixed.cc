/*  
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180322
 *  Updated: 20180322
 *
 *  Purpose: try code involving _PointMover
 */
#include <iostream>
#include "../ArgvParser_forSimulator.hh"
#include "../PointMover_1fixed.hh"



int main( int argc, const char* argv[] ){
  ArgvParser_forSimulator argvP(  "distPerMove1  distPerMove2", argc, argv  );

  const double distPerMove1=  argvP.argVal<double>(0);
  const double distPerMove2=  argvP.argVal<double>(1);

  argvP.close();

  PointMover_1fixed pointMover;
  pointMover.set_MSDperMove( distPerMove1 );
    
  Point point( 3.0, 5.0, 6.0 );

  cout  <<  "Moving point with distPerMove= "  <<  distPerMove1  <<  endl;
  for(  size_t i= 0; i < 5; ++i  ){
    pointMover.move( point );
    cout  <<  "Point at: "  <<  point  << endl;
  }

  pointMover.set_MSDperMove( distPerMove2 );
  cout  <<  "\nMoving point with distPerMove= "  <<  distPerMove2  <<  endl;
  for(  size_t i= 0; i < 5; ++i  ){
    pointMover.move( point );
    cout  <<  "Point at: "  <<  point  << endl;
  }
  
  return 1;
}

