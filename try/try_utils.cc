/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created:  20170228
 *  Modified: 20170603
 *
 *  Purpose: try code involving utils
 */
#include "../utils.hh"

using namespace utils;


int main( int argc, const char* argv[] ){

  const double n= 3.0;
  const double p= 0.3;

  printf(  "With n=%g, p=%g,  The chance of at least one success is %g\n",
           n, p,  prob_atLeastOneSuccess( n, p )
           );

  printf(  "justReturnArg2( %g, %g ) returned %g\n",
           n, p,  justReturnArg2( n, p )
           );
  
  //                         0  1  2  3  4  5  6  7  8  9
  vector<size_t> numTimes=  {5, 8, 3, 2, 4, 2, 0, 3, 8, 9, 2};

  double m=  mean( numTimes );

  double d=  stDev( numTimes );

  printf( "mean= %g±%g\n", m, d  );

  cout  <<  "\ndeciles:\n"
        <<  asString( deciles(numTimes) )   <<  endl;


  const string phrase(  "a\tman\ta\tplan\ta\tcanal\n"  );
  stringDQ_T words=  splitOnTabs( phrase );

  cout  <<  "words from " <<  phrase  <<  " are:\n";
  for(  const auto& word :  words  ){
    cout  <<  "  "  <<  word;
  }
  cout << "\n";


  if(  deletedOneFromStringsP( words, string("man") )  ){
    cout  <<  "deleted 'man' from words\n";
  }
  else{
    cout  <<  "Internal Error\n";
  }

  cout  <<  "now words are:\n";
  for(  const auto& word :  words  ){
    cout  <<  "  "  <<  word;
  }
  cout << "\n";
  

  const string spec(  "3:gauss:dist"  );
  const stringDQ_T terms=  splitOnColons( spec );

  cout  <<  "terms from " <<  spec  <<  " are:\n";
  for(  const auto& term :  terms  ){
    cout  <<  "  "  <<  term;
  }
  cout << "\n";

  const doubleVecT& prop1=  {0.3, 0.5, 0.2, 0.7};
  const doubleVecT& prop2=  {0.2, 0.6, 0.3, 0.8};

  printf(  "meanAbsError is %g\n",  meanAbsError( prop1, prop2 )  );


  const string okayStrings(  "goodApple|badApple"  );

  const stringDQ_T stringsToTest
  {  "GOODAPPLE", "badapple", "badAple"  };

  for(  auto& s  :  stringsToTest  ){
    if(   any_eqciP( s, okayStrings )  ){
      printf(  "String '%s' matched set '%s'\n", C(s), C(okayStrings)  );
    }else{
      printf(  "Expected string matching set '%s', but got '%s'\n", C(okayStrings), C(s)  );
    }
  }
}
