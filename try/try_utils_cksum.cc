/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created:  20170228
 *  Modified: 20170603
 *
 *  Purpose: try code involving utils
 */
#include "../utils.hh"

using namespace utils;


int main( int argc, const char* argv[] ){
  string usage(  string("Usage: ") + argv[0] + " pathname\n"  );

  if(  argc != 2  ){
    cout  <<  usage;  exit( 64 );
  }

  cout  <<  shell_cksum( argv[1] );
}
