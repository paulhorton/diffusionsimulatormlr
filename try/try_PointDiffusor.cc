/*  
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180322
 *  Updated: 20180322
 *
 *  Purpose: try code involving PointDiffusor
 */
#include <iostream>
#include "../ArgvParser_forSimulator.hh"
#include "../PointDiffusor.hh"



int main( int argc, const char* argv[] ){
  ArgvParser_forSimulator argvP(  "diffuserSpec DC1 DC2", argc, argv  );

  const string diffuserSpec=        argvP(0);
  const double DC1=  argvP.argVal<double>(1);
  const double DC2=  argvP.argVal<double>(2);
  argvP.close();

  PointDiffusor pointDiffusor(  diffuserSpec  );

  pointDiffusor.setDiffusionCoeff( DC1 );

  RetractablePoint point;
  cout  <<  "point starting at "  <<  point  <<  endl;
  
  pointDiffusor.moveOneStep( point );
  cout  <<  "after one move (DC="<<DC1<<") point at "  <<  point  <<  endl;

  for(  size_t i= 0;  i < 6;  ++i  ){
    pointDiffusor.moveOneStep( point );
    cout  <<  "after another move point at "  <<  point  <<  endl;
  }
  cout  <<  "elsaped time is: "  <<  pointDiffusor.elapsedTime()  <<  endl;
  

  pointDiffusor.setDiffusionCoeff( DC2 );  
  pointDiffusor.moveOneStep( point );
  cout  <<  "after one move (DC="<<DC2<<") point at "  <<  point  <<  endl;
  cout  <<  "elsaped time is: "  <<  pointDiffusor.elapsedTime()  <<  endl;


  return 1;
}
