/*  
 *  Author: Paul Horton
 *  Creation Date: 20170601
 *  Last Modified: 20170601
 *
 *  Purpose: try code involving StepLengthComputer.
 */
#include "../ArgvParser.hh"
#include "../StepLengthComputer.hh"



int main(  int argc,  const char* argv[]  ){
  ArgvParser argvParser(  "diffusionCoefficient numDimsMovedPerStep varyTime|varyGrid baseLength",  argc, argv  );

  const double DC=                  argvParser.argVal<double>(0);
  const size_t numDimsMovedPerStep= argvParser.argVal<size_t>(1);
  const string whatToVary=          argvParser.argVal<string>(2);
  const double baseLength=          argvParser.argVal<double>(3);

  const StepLengthComputer *const stepLengthComputer=
    StepLengthComputer::newInstance( numDimsMovedPerStep, whatToVary, baseLength );


  double gridStepLength, timeStepLength;
  stepLengthComputer->set_gridAndTimeStepLength_forDC(  gridStepLength, timeStepLength, DC  );
  printf(  "gridStepLength= %g,  timeStepLength= %g,  DC= %g\n",  gridStepLength, timeStepLength,  DC  );
}

