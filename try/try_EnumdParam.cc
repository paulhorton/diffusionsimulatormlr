/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Creation Date: 2017.2.28
 *  Last Modified: $Date: 2012/04/04 11:48:03 $
 *
 *  Purpose: try code involving EnumdParam.hh
 */
#include "../ArgvParser.hh"
#include "../EnumdParam.hh"



int main(  int argc,  const char* argv[]  ){
  ArgvParser args(  "initDataString",  argc, argv  );

  EnumdParam param=  EnumdParam::make( args(0) );

  cout  <<  param  <<  endl;
  
  for(  ;
        param.inRangeP();
        param.next()  ){
    cout  <<  param()  <<  endl;
  }

  
  cout  <<  "Now we assert all values are probabilities...\n";

  param.assertAllValidProbs();

  cout  <<  "Indeed they are!\n";
}
