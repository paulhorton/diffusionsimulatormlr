/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Creation Date: 2017.2.24
 *  Last Modified: $Date: 2012/04/04 11:48:03 $
 *
 *  Purpose: try code involving MobilityParams
 */
#include "../ArgvParser.hh"
#include "../MobilityParams.hh"


int main(  int argc,  const char* argv[]  ){
  ArgvParser args(  "freeStride trslStride freeAnchorProb trslAnchorProb",  argc, argv  );

  const MobilityParams mobilityParams(  args(0), args(1), args(2), args(3) );

  cout  <<  "mobilityParams: "  <<  mobilityParams  <<  endl;
}

