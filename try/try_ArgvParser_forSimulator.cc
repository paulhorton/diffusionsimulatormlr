/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created: 20170606
 *  Updated: 20170606
 *
 *  Purpose: try code involving ArgvParser_forSimulator
 */
#include <iostream>
#include "../ArgvParser_forSimulator.hh"





int main( int argc, const char* argv[] ){
  ArgvParser_forSimulator argvP( "-s stride", argc, argv );

  const double stride=  argvP.optVal<double>( "-s" );

  argvP.close();

  cout  <<  "Stride is "  <<  stride  <<  endl;
}

