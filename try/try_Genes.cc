/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  Purpose: try code involving Genes
 */
#include "../Genes.hh"


int main(  int argc,  const char* argv[]  ){
  const string  usage(  string(argv[0])  +  " genesInfoFile"  );

  if(  argc != 2  ){
    printf(  "Expected 1 arg but got %d\nUsage: %s\n",   argc-1,  C(usage)   );
    exit( 64 );
  }

  Genes genes(  argv[1]  );

  cout  <<  "Outputing constructed Genes object...\n"  <<  genes;
}

