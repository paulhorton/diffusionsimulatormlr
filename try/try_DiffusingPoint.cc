/*  
 *  Author: Paul Horton
 *  Creation Date: 20180305
 *  Last Modified: 20180305
 *
 *  Purpose: try code involving DiffusingPoint
 */
#include "../ArgvParser.hh"
#include "../utils.hh"
#include "../DiffusingPoint.hh"



int main(  const int argc,  const char* argv[] ){

  ArgvParser  argvParser(  "numSteps spaceStepSize",  argc, argv  );

  const size_t numSteps      = argvParser.argVal<size_t>( 0 );
  const double spaceStepSize = argvParser.argVal<double>( 1 );

  argvParser.close();


  DiffusingPoint diffusingPoint( spaceStepSize );


  ASSERTF(  numSteps >   0,  "Should do a positive number of steps, but %zu steps specified on command line",   numSteps  );
  ASSERTF(  numSteps < 101,  "Do not want to do more than 100 steps, but %zu steps specified on command line.", numSteps  );
  

  utils::seedRandom();
  for(  size_t step_i= 0;  step_i < numSteps;  ++step_i  ){
    diffusingPoint.walkOneRandomStep();
    if(  utils::happensP( 0.3 )  ){
      diffusingPoint.undoPreviousStep();
      cout  <<  "undoing step "  <<  step_i  <<  endl;
    }
    cout  <<  "At step "  <<  step_i  <<  " point located at "  <<  diffusingPoint  <<  endl;
  }
}


