/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created: 20170605
 *  Updated: 20170605
 *
 *  Purpose: try code involving Cytoplasm
 */
#include <iostream>
#include "../Cytoplasm.hh"



int main( int argc, const char* argv[] ){

  Cytoplasm cyto;

  cyto.resetForNewWalk();

  cyto.printMitos();

}

