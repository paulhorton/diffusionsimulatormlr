/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Creation Date: 2017.2.28
 *  Last Modified: $Date: 2012/04/04 11:48:03 $
 *
 *  Purpose: try code involving XableParam.hh
 */
#include "../XableParam.hh"



int main(  int argc,  const char* argv[]  ){
  const stringDQ_T args( argv, argv+argc );
  const string  usage(  args[0]  +  " initVal minVal multiplier"  );

  if(  args.size() !=  4  ){
      printf(  "Expected 3 args but got %zu\nUsage: %s\n",   args.size()-1, usage.c_str()  );  exit( 64 );
  }

  double initVal, minVal, multiplier;
  try{
    initVal     =lexical_cast<double>( args[1] );
    minVal      =lexical_cast<double>( args[2] );
    multiplier  =lexical_cast<double>( args[3] );
  }
  catch(  const bad_lexical_cast&  ){
    printf(  "Could not parse arguments.\n"  );
    printf(  "Usage: %s\n", C(usage)  );   exit( 64 );
  }


  XableParam param( "dummy Xable", initVal, minVal, multiplier );

  cout  <<  param  <<  endl;
  
  for(  ;
        param.inRangeP();
        param.next()  ){
    printf(  "%g\n", param()  );
  }
  
}
