/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *  Last Modified: $Date: 2012/04/04 11:48:03 $
 *
 *  Purpose: try code involving Gene
 */
#include "../ArgvParser.hh"
#include "../Gene.hh"


int main(  int argc,  const char* argv[]  ){
  ArgvParser argvParser(  "ID avgTrslInitTime avgNumRibosomes",  argc, argv  );

  const string name=  argvParser(0);
  const double avgTrslInitTime=  argvParser.argVal<double>( 1 );
  const double avgNumRibosomes=  argvParser.argVal<double>( 2 );

  Gene gene(  name,  avgTrslInitTime,  avgNumRibosomes  );

  cout  <<  "gene : "  <<  gene.ID()  <<  "constructed\n";

  cout  <<  "using ostream gene prints as\n  " <<  gene  <<  endl;
}

