/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Creation Date: 2017.2.28
 *  Last Modified: $Date: 2012/04/04 11:48:03 $
 *
 *  Purpose: try code involving utils
 */
#include "../utils.hh"



int main( int argc, const char* argv[] ){
  stringDQ_T args( argv, argv+argc );
  const string  usage(  args[0]  +  " numbers"  );

  if(  args.size() <  2  ){
    printf(  "Expected at least one arg but got none.\nUsage: %s\n",   C(usage)   );
    exit( 64 );
  }

  args.pop_front();

  doubleVecT doubleVec=
    utils::toDoubleVec(  "Vector read in when running try_utils_toDoubleVec.",  args  );

  printf(  "Resulting vector of doubles is:"  );
  for(  double& val :  doubleVec  ){
    printf(  "\t%g", val  );
  }
  printf(  "\n"  );
}
