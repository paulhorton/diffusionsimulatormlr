/*  
 *  Author: Paul Horton
 *  Creation Date: 20170601
 *  Last Modified: 20170601
 *
 *  Purpose: try code involving Sphere
 */
#include "../Sphere.hh"



int main(  int argc,  const char* argv[]  ){
  if(  argc > 1   ){
    printf(  "No arguments needed\n"  );
    exit( 64 );
  }

  srand(  (unsigned) time(NULL)  );

  Sphere cell(  Point(0,0,0), 2.5  );

  cout  <<  "cell is: "  <<  cell  <<  endl;


  vector<Sphere> mitos( 5 );

  for(  auto mito  :  mitos  ){
    mito.setRadius(  utils::randomNumber_fromRange( 0.25, 0.50 )  );
    mito.placeRandomly_withinOriginCenteredSphericalShell(  1.0, 2.5  );
    cout  <<  "mito is: "  <<  mito
          <<  "\tdistance to origin is: "  <<  mito.center().distanceToOrigin()  <<  endl;
  }

}
