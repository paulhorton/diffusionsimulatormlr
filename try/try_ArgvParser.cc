/*  
 *  Author: Paul Horton
 *  Copyright (C) 2017, Paul Horton, All rights reserved.
 *  Created: 20170606
 *  Updated: 20170606
 *
 *  Purpose: try code involving ArgvParser
 */
#include <iostream>
#include "../ArgvParser.hh"





int main( int argc, const char* argv[] ){
  ArgvParser argvP( "[-v] [-t temperature] numPlanets", argc, argv );

  cout  <<  "Trying out ArgvParser with usage '"  <<  argvP.usage()  <<  "'\n";

  cout  <<  "Number of command line args given = "  <<  argvP.size()  <<  endl;

  const bool verbose=  argvP.flagged( "-v" );

  const double temperature=  argvP.optVal<double>( "-t", 451 );

  const size_t numPlanets=  argvP.argVal<size_t>( 0 );


  argvP.close();

  cout  <<  "Verbose is "  <<  verbose  <<  endl;
  cout  <<  "The temperature is "  <<  temperature  <<  endl;
  cout  <<  "The number of planets is: "  <<  numPlanets  <<  endl;
}

