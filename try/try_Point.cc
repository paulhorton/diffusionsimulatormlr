/*  
 *  Author: Paul Horton
 *  Creation Date: 20170601
 *  Last Modified: 20170601
 *
 *  Purpose: try code involving Point
 */
#include "../ArgvParser.hh"
#include "../Point.hh"



int main( int argc, const char* argv[] ){
  ArgvParser argvP(  "x y z",  argc, argv  );

  const double x=  argvP.argVal<double>(0);
  const double y=  argvP.argVal<double>(1);
  const double z=  argvP.argVal<double>(2);

  argvP.close();


  Point punkt1( 3.5, 6, 2 );

  cout  <<  "Point1 was: "  <<  punkt1  <<  endl;

  punkt1.moveTo(  Point( x, y, z )  );
  cout  <<  "Point 1 moved it to "  <<  punkt1  <<  endl;

  cout  <<  "The distance from Point1 to the origin is: "
        <<  punkt1.distanceToOrigin()  <<  endl;


  const Point punkt2( 2, 3, 1 );

  cout  <<  "Point2 is: "  <<  punkt2  <<  endl;

  cout  <<  "\nThe distance from "  <<  punkt1  <<  " to "  <<  punkt2  <<  " is: "
        <<  punkt1.distanceTo( punkt2 )  <<  endl;


  const Point punkt3=  punkt2;
  cout  <<  "punkt3=  punkt2, sets point3 to "  <<  punkt3  <<  endl;

  const string maybe=  punkt1.withinOriginCenteredSphericalShellP( 1, 2.5 )?  "" :  " not";

  cout  <<   "point: "  <<  punkt1  <<  " is"  <<  maybe  <<  " within spherical shell r=1, R=2.5\n";
}
