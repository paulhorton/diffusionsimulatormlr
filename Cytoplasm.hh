/*  
 *  Author: Paul Horton
 *  Creation Date: 20170224
 *
 *  Description:
 *      Simplified cytoplasm containing bounded by constant 
 *
 *  Purpose:  Part of reimplementation of a simulator conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>
#include "RetractablePoint.hh"
#include "Sphere.hh"
#include "PointDiffusor.hh"



class Cytoplasm{
public:
  Cytoplasm()
    : mRNA(), mitos(5)
  {}


  /*────────────────────  Public Object Data  ────────────────────*/
  RetractablePoint mRNA;


  bool  mRNAinCytoplasmP() const{   return  mRNA.withinOriginCenteredSphericalShellP (nuclRadius,cellRadius);   }

  //  If mRNA is in a mitochondria, return its serial number (≧1), otherwise return 0.
  size_t mRNAinMitoP() const;

  void place_mRNA_at(  const Point& point  ){
    mRNA.moveTo(  point  );
  }

  void resetForNewWalk(){
    static const Point nuclExitPoint(1.0, 0, 0);
    mRNA.moveTo(  nuclExitPoint  );
    randomlyPlaceMitos();
  }

  void printMitos()  const{
    for(  const auto& mito  :  mitos  ){
      cout  <<  mito  <<  endl;
    }
  }
  

private:

  /*────────────────────  Constants  ────────────────────*/
  // Distances in microns.
  static constexpr double cellRadius=     2.50;
  static constexpr double mitoRadiusMin=  0.25;
  static constexpr double mitoRadiusMax=  0.50;
  static constexpr double nuclRadius=     1.00;


  /*────────────────────  Private Methods  ────────────────────*/
  //  Randomly place mitochondria with random radius ∈ [mitoRadiusMin, mitoRadiusMax], under the constraint that they are contained in the cytoplasm and do not overlap with each other.
  void randomlyPlaceMitos();


  /*────────────────────  Private Object Data  ────────────────────*/
  vector<Sphere>   mitos;
};// class Cytoplasm
