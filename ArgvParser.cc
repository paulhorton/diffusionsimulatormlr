
//  See header file.
#include "ArgvParser.hh"




ArgvParser::ArgvParser(  const string& _usage,
                         const int argc, const char* argv[]
                         )
  : usageSpec( _usage ),
    argvCopy(  argv, argv+argc  ),
    args  (  argv+1, argv+argc  ),
    used  (  argc-1,  never    ),
    argValCalledP(  false  ),
    progName(  argv[0]  )
{
  // Mark first occurrence of '--' as used, to prevent it looking like a positional arg.
  for(  size_t i=0;  i < args.size();  ++i  ){
    if(  args[i] == "--"  ){
      used[i]=  named;   break;
    }
  }
}


string ArgvParser::usage() const{
  if(   usageSpec.find('\n')  ==  string::npos   ){
    return  string(  "Usage: "  +progName  +' ' + defaultOptions() + usageSpec  +'\n'  );
  }
  else{//  passed "hand crafted" multiline usage as is.
    return  usageSpec;
  }
}


//  Return index of nth unused argument.
//  idx counts from one.
size_t ArgvParser::nth_unused_asNamed_idx(  const size_t n  )  const{
  size_t unusedAsNamedCount= 0;

  for(  size_t i=0;  i < args.size();  ++i  ){
    if(  used[i] != named        )   ++unusedAsNamedCount;
    if(  unusedAsNamedCount > n  )               return i;
  }

  cout  <<  "Command Line Parsing Error: expected at least "  <<  n+1  <<  " positional args, but found only "  <<  unusedAsNamedCount  <<  endl
        <<  usage();
  exit(  64  );
}


//  Return index of value of option preceded by optName.
//  idx_of_optVal( "-r" ) should return 2 for command line like: progName 39 -r 29 fred...
//  Returns zero if optName not found or is used.
size_t ArgvParser::idx_of_optVal(  const string& optName  )  const{

  if(   args.size()  <  2   )    return 0;

  for(  size_t i=0;   i <  args.size()- 1;   ++i  ){
    if(  args[i] == string("--")  )      return  0;
    if(  used[i]                  )      continue;
    if(  args[i] == optName       ){
      if(  used[i+1]  ){
          cout  <<   "Confused while tried to parse command line.  Arg '"  <<  args[i+1]  <<  "' looks like it is the value of the preceding arg '"  <<  args[i]  <<  "' but apparently it has already been used.\n";
          exit(  1  );
      }
      return  i+1;
    }
  }
   return  0;
}


bool ArgvParser::nameFlagged(  const string& flagName  ){

  for(  size_t i=0;  i < args.size();  ++i  ){
    if(  args[i] == string("--")  )      return  false;
    if(  used[i]                  )      continue;
    if(  args[i] == flagName      ){
      used[i]=  named;
      return  true;
    }
  }   
  return false;
}


bool ArgvParser::flagged(  const stringDQ_T& flagAliases  ){
  
  for(  const string& alias  :  flagAliases  ){
    if(  nameFlagged( alias )  ){
      return  true;
    }
  }
  return  false;
}


//  Called to indicate argv parsing should be finished.
//  Exits with error if some args remain unused.  Otherwise does nothing.
void ArgvParser::close(){

  vector<string> unused;
  for(  size_t i=0;  i < args.size();  ++i  ){
    if(  !used[i]  )   unused.push_back( args[i] );
  }

  if(  unused.size()  ){
    cout  <<  "Command Line Parsing Error; unused args:";
    for(  const string& arg  :  unused  ){
      cout  <<  ' '  <<  arg;
    }
    cout  <<  endl;
    exit( 64 );
  }
}
