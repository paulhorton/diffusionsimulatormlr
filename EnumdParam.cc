/*
 *  Author: Paul Horton
 *  Creation Date: 2017.3.22
 *  Description: See header file.
 */
#include "EnumdParam.hh"



EnumdParam EnumdParam::make(  const string dataInfo  ){
  
  stringDQ_T fields=  utils::splitOnTabs(  dataInfo  );

  ASSERTF(  fields.size() > 1,
            "When constructing EnumdParam; passed string '%s'\nwith less than two tab separated fields\n",  C(dataInfo)
            );

  const string name=  fields[0];
  fields.pop_front();

  const doubleVecT vals=  utils::toDoubleVec(  "When constructing EnumParam " + name,  fields  );

  return  EnumdParam(  name, vals  );
}
