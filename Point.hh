/*
 *  Author: Paul Horton
 *  Creation Date: 20170224
 *
 *  Description: Point in 3D space.
 *
 *  Purpose:  Created as part of reimplementation of a simulator conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"
#include "utils.hh"


class Point{
public:
  /*───────────────  CONSTRUCTORS  ───────────────*/
  Point() : x(0), y(0), z(0)
  {}

  Point(  double x_, double y_, double z_  ) : x(x_), y(y_), z(z_)
  {}



  /*───────────────  ACCESSORish METHODS ───────────────*/

  void copyCoordsTo(  double& xCopy, double& yCopy, double& zCopy  ){
    xCopy= x;  yCopy= y;  zCopy= z;
  }


  // Derived classes may keep history of point.
  virtual void moveTo(  double xNew, double yNew, double zNew  ){
    x= xNew;  y= yNew;  z= zNew;
  }

  virtual void moveTo(  const Point& point  ){
    x= point.x;  y= point.y;  z= point.z;
  }



  /*───────────────  OTHER METHODS ───────────────*/

  double distanceToOrigin()  const{
    return   sqrt(  x*x  +  y*y  +  z*z  );
  }

  double distanceTo(  const Point& point  )  const{
    return   sqrt(   pow(  (x - point.x),  2  )  +
                     pow(  (y - point.y),  2  )  +
                     pow(  (z - point.z),  2  )   );
  }

  // Return true iff point is in spherical shell bounded by inner, outer radii (r, R)
  bool withinOriginCenteredSphericalShellP(  const double r,  const double R  )  const{
    const double d=  distanceToOrigin();
    return
      (r <= d)  &&  (d <= R);
  }

  // Place oneself in random spot enclosed in cube with corners (±d,±d,±d).
  void moveToRandomPoint_inOriginCenteredCube(  const double d  ){
    x=  utils::randomNumber_fromRange( -d, +d  );
    y=  utils::randomNumber_fromRange( -d, +d  );
    z=  utils::randomNumber_fromRange( -d, +d  );
  }

  string  asString() const{
    std::ostringstream buffer;
    buffer  <<  numberFormat  <<  x  <<  ';'
            <<  numberFormat  <<  y  <<  ';'
            <<  numberFormat  <<  z;
    return buffer.str();
  }

  
  friend ostream& operator<<(  ostream& os, const Point& point  );
  
private:
  static ostream& numberFormat( ostream& os ){
    return
      os  <<  std::setprecision(4)  <<  std::showpos  <<  std::setw(7)  <<  std::left  <<  std::fixed;
  }

protected:
  /*───────────────  OBJECT DATA  ───────────────*/
  double x, y, z;
};


inline ostream& operator<<(  ostream& os, const Point& point  ){
  return
    os  <<  point.numberFormat  <<  point.x  <<  ';'
        <<  point.numberFormat  <<  point.y  <<  ';'
        <<  point.numberFormat  <<  point.z;
}
