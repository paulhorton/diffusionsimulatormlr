/*  
 *  Author: Paul Horton
 *  Creation Date: 20170224
 *
 *  Description: implementation of PointMover.
 *
 */
#pragma once
#include "PointMover.hh"


class PointMover_1fixed : public PointMover{
public:

  void move(  Point& point  ) override;

  void set_MSDperMove(  double MSDperMove  ) override{
    distPerMove=  sqrt( MSDperMove );
  }

private:
  double distPerMove=  0.0;  // Distance to move the point each move.
};
