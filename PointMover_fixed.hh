/*  
 *  Author: Paul Horton
 *  Creation Date: 20170319
 *
 *  Description: Implementation of PointMover which
 *               moves the point a fixed distance
 *               along each axis.
 *
 */
#pragma once
#include "PointMover.hh"


class PointMover_fixed : public PointMover{
public:

  void move(  Point& point  ) override;

  void set_MSDperMove(  double MSDperMove  ) override{
    distToMovePerAxis=  sqrt( MSDperMove / 3.0 );
  }

private:
  double distToMovePerAxis=  0.0;  // Distance to move along each axis each move.
};
