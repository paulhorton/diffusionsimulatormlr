/*  
 *  Author: Paul Horton
 *  Creation Date: 2017.2.24
 *
 *  Description: Class to hold statistics about what has happened to a gene.
 *
 *  Purpose: Created as an extension of a random walk simulation for mRNA molecules conceived of by Thomas Poulsen.
 */
#pragma once
#include "commonMacrosUsingTypedefs.hh"



class GeneStats{
public:

  /*───────────────  CONSTRUCTORS  ───────────────*/
  GeneStats(  const string& ID  );

  /*───────────────  ACCESSORS  ───────────────*/
  const string&  ID() const{  return _ID;  }

  size_t numAnchorings() const{
    return  accumulate(   ALLOF(_numAnchorings_free),  0   )
      +     accumulate(   ALLOF(_numAnchorings_trsl),  0   );
  }

  size_t numMitoHits() const{
    return  accumulate(   ALLOF(_numAnchorings_free_attempted),  0   )
      +     accumulate(   ALLOF(_numAnchorings_trsl_attempted),  0   );
  }


  /*───────────────  OTHER METHODS  ───────────────*/
  void recordAnchoringAttempt(  const bool translatingP,  const size_t numPrevAnchAttempts  ){
    const size_t curAttemptNumIdx=  numAttemptsIdx(  numPrevAnchAttempts  );
    if(  translatingP  ){   ++_numAnchorings_trsl_attempted.at( curAttemptNumIdx );    }
    else{                   ++_numAnchorings_free_attempted.at( curAttemptNumIdx );    }
  }

  void recordAnchoring       (  const bool translatingP,  const size_t numPrevAnchAttempts  ){
    const size_t curAttemptNumIdx=  numAttemptsIdx(  numPrevAnchAttempts  );
    if(  translatingP  ){   ++_numAnchorings_trsl          .at( curAttemptNumIdx );    }
    else{                   ++_numAnchorings_free          .at( curAttemptNumIdx );    }
  }

  void printStats() const;

  void zeroStats(){
    std::fill(  ALLOF(_numAnchorings_free),            0  );
    std::fill(  ALLOF(_numAnchorings_free_attempted),  0  );
    std::fill(  ALLOF(_numAnchorings_trsl),            0  );
    std::fill(  ALLOF(_numAnchorings_trsl_attempted),  0  );
  }


  size_t numAttemptsIdx(  const size_t numAttempts  ) const{
    return  std::min(  numAttempts,  _numAttemptsIdxMax  );
  }

  /*───────────────  OBJECT DATA  ───────────────*/

private:
  const string  _ID;
  const size_t  _numAttemptsIdxMax;

  vector<size_t>  _numAnchorings_free_attempted;
  vector<size_t>  _numAnchorings_trsl_attempted;
  vector<size_t>  _numAnchorings_free;
  vector<size_t>  _numAnchorings_trsl;

};
