/*
 *  Author: Paul Horton
 *  Copyright (C) 2018, Paul Horton, All rights reserved.
 *  Created: 20180318
 *  Updated: 20180318
 *  Description: See header file.
 */
#include "PointMover_fixed.hh"


void PointMover_fixed::move(  Point& point  ){

  double newX, newY, newZ;
  point.copyCoordsTo(  newX, newY, newZ  );

  const double r=  utils::randomNumber_fromRange(  0.0, 8.0  );

  if(  r < 4  ){
    newX +=  distToMovePerAxis;
    if(  r < 2  ){                         // [0,2)
      newY +=  distToMovePerAxis;
      newZ +=  (r < 1)?  distToMovePerAxis  :  -distToMovePerAxis;
    }
    else{                                  // [2,4)
      newY -=  distToMovePerAxis;
      newZ +=  (r < 3)?  distToMovePerAxis  :  -distToMovePerAxis;
    }
  }
  else{  // r ≧ 4
    newX -=  distToMovePerAxis;
    if(  r < 6 ){                          // [4,6)
      newY +=  distToMovePerAxis;
      newZ +=  (r < 5)?  distToMovePerAxis  :  -distToMovePerAxis;
    }
    else{                                  // [6,8]
      newY -=  distToMovePerAxis;
      newZ +=  (r < 7)?  distToMovePerAxis  :  -distToMovePerAxis;
    }
  }

  point.moveTo(  newX, newY, newZ  );
}
