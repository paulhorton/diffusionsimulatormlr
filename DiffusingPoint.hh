/*  
 *  Author: Paul Horton
 *  Creation Date: 20170224
 *
 *  Description: Functions for a 3D random walk in a yeast cell.
 *
 *  Purpose:  Part of reimplementation of a simulator conceived of and written by Thomas Poulsen,
 *            in the context of a research paper
 *            "Hallmarks of slow translation initiation revealed in mitochondrially localizing mRNA sequences",
 *            by T. Poulsen, K. Imai, M. Frith and P. Horton.
 *
 */
#pragma once
#include "Point.hh"



class DiffusingPoint : public Point{
public:
  /*───────────────  CONSTRUCTORS  ───────────────*/
  DiffusingPoint(  const double strideLength= -1  )
    :
    stride(strideLength)
  {}


  DiffusingPoint(  const double x_,  const double y_,  const double z_  )
    :
    stride(-1)            // Dummy init value
  {}


  /*───────────────  ADDITIONAL METHODS  ───────────────*/
  void setStrideLength(  const double strideLength  ){
    stride=  strideLength;
  }

  void walkOneRandomStep();  // Blindly take one step in random direction.

  void undoPreviousStep();   // Undo most recently taken random step.

private:
  /*──────────  ADDITIONAL OBJECT DATA  ───────────*/
  double prevX, prevY, prevZ;
  double stride;

  //  Machinery to ensure undoPreviousStep is only called directly after a call to walkOneRandomStep.
#ifndef NDEBUG
  enum class methodName {constructor, walkOneRandomStep, undoPreviousStep};
  methodName prevCalledMethod= methodName::constructor;

  static const char* methodAsCStr( const methodName& method ){
    return(
           (method == methodName::constructor)?        "constructor"        :
           (method == methodName::walkOneRandomStep)?  "walkOneRandomStep"  :
           (method == methodName::undoPreviousStep)?   "undoPreviousStep"   :
           /**/                                        "UNKNOWN"
           );
  }
#endif
};
